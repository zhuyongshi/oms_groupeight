package com.aaa.sbms.controller;

import com.aaa.sbms.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * className:DeptController
 * description:
 * author:zys
 * createTime:2020/6/15 0015 22:07
 */
@RestController
@RequestMapping("dept")
public class DeptController {

    @Autowired
    private DeptService deptService;
    /**
      * @Description:获取部门数据
      * @Author zys
      * @Date 2020/6/16 0016 13:11
      * @Param [map]
      * @return java.lang.Object
     */
    @RequestMapping("page")
    public Object getDeptList(@RequestParam Map map){
        return deptService.getDeptList(map);
    }
    /**
      * @Description: 更新部门数据
      * @Author zys
      * @Date 2020/6/16 0016 13:11
      * @Param [map]
      * @return java.lang.Object
     */
    @RequestMapping("update")
    public Object update(@RequestBody Map map){
        return deptService.update(map);
    }

    /**
      * @Description:添加部门信息
      * @Author zys
      * @Date 2020/6/16 0016 14:10
      * @Param [map]
      * @return java.lang.Object
     */
    @RequestMapping("add")
    public Object add(@RequestBody Map map){
        return deptService.add(map);
    }

    @RequestMapping("delete")
    public Object delete(Integer deptId){
        return deptService.delete(deptId);
    }
}
