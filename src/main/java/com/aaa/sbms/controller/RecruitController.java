package com.aaa.sbms.controller;

import com.aaa.sbms.service.RecruitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * className:RecruitController
 * description:
 * author:zys
 * createTime:2020/6/25 0025 11:06
 */
@RestController
@RequestMapping("recruit")
public class RecruitController {

    @Autowired
    private RecruitService recruitService;

    /**
      * @Description: 添加简历
      * @Author zys
      * @Date 2020/6/28 0028 21:21
      * @Param [map]
     */
    @RequestMapping("addResume")
    public Object addResume(@RequestBody Map map){
        return recruitService.addResume(map);
    }

    /**
      * @Description: 获取招聘信息
      * @Author zys
      * @Date 2020/6/25 0025 11:10
      * @Param [map]
     */
    @RequestMapping("page")
    public Object listInfo(@RequestParam Map map){
     return recruitService.listInfo(map);
    }

    /**
      * @Description: 更新招聘信息状态
      * @Author zys
      * @Date 2020/6/25 0025 12:57
      * @Param [id]
     */
    @RequestMapping("delete")
    public Object updateStateById(Integer id){
        return recruitService.updateStateById(id);
    }

    /**
      * @Description: 更新信息
      * @Author zys
      * @Date 2020/6/25 0025 13:25
      * @Param [map]
     */
    @RequestMapping("update")
    public Object update(@RequestBody Map map){
        return recruitService.update(map);
    }


    /**
     * @Description: 添加信息
     * @Author zys
     * @Date 2020/6/25 0025 13:25
     * @Param [map]
     */
    @RequestMapping("add")
    public Object add(@RequestBody Map map){
        return recruitService.add(map);
    }


    /**
      * @Description: 入职审核记录
      * @Author zys
      * @Date 2020/6/25 0025 14:14
      * @Param [map]
     */
    @RequestMapping("recordList")
    public Object recordList(@RequestParam Map map){
        return recruitService.recordList(map);
    }

    /**
      * @Description: 获取待审核应聘信息
      * @Author zys
      * @Date 2020/6/25 0025 15:44
      * @Param [map]
     */
    @RequestMapping("check")
    public Object listCheck(@RequestParam Map map){
        return recruitService.listCheck(map);
    }

    /**
      * @Description: 审批
      * @Author zys
      * @Date 2020/6/25 0025 15:59
      * @Param [map]
     */
    @RequestMapping("pass")
    public Object pass(@RequestBody Map map){
        return recruitService.pass(map);
    }

    /**
      * @Description: 驳回申请
      * @Author zys
      * @Date 2020/6/25 0025 16:06
      * @Param [map]
     */
    @RequestMapping("unPass")
    public Object unPass(@RequestBody Map map){
        return recruitService.unPass(map);
    }

    /**
      * @Description: 入职新员工
      * @Author zys
      * @Date 2020/6/25 0025 16:28
      * @Param [map]
     */
    @RequestMapping("newList")
    public Object newList(@RequestParam Map map){
        return recruitService.newList(map);
    }
    /**
      * @Description: 驳回新员工入职
      * @Author zys
      * @Date 2020/6/25 0025 17:26
      * @Param [map]
     */
    @RequestMapping("leave")
    public Object leave(Integer pId){
        return recruitService.leave(pId);
    }

    /**
      * @Description: 入职新员工
      * @Author zys
      * @Date 2020/6/25 0025 18:00
      * @Param [map]
     */
    @RequestMapping("addEmp")
    public Object addEmp(@RequestBody Map map){
        return recruitService.addEmp(map);
    }
}
