package com.aaa.sbms.controller;

import com.aaa.sbms.service.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * className:HouseController
 * description:
 * author:zys
 * createTime:2020/6/19 0019 12:21
 */
@RestController
@RequestMapping("house")
public class HouseController {

    @Autowired
    private HouseService houseService;
    /**
      * @Description: 获取宿舍信息
      * @Author zys
      * @Date 2020/6/19 0019 16:37
      * @Param [map]
      * @return java.lang.Object
     */
    @RequestMapping("page")
    public Object ListHouse(@RequestParam Map map){
        return houseService.listHouse(map);
    }
    /**
      * @Description:添加宿舍
      * @Author zys
      * @Date 2020/6/19 0019 16:38
      * @Param [map]
      * @return java.lang.Object
     */
    @RequestMapping("add")
    public Object addHouse(@RequestBody Map map){
        return houseService.addHouse(map);
    }

    /**
      * @Description:更新宿舍
      * @Author zys
      * @Date 2020/6/19 0019 20:35
      * @Param [map]
     */
    @RequestMapping("update")
    public Object updateHouse(@RequestBody Map map){
        return houseService.updateHouse(map);
    }

    /**
      * @Description: 获取房间号
      * @Author zys
      * @Date 2020/6/20 0020 10:51
      * @Param [floor, houseId]
     */
    @RequestMapping("getRooms")
    public Object getRooms(Integer floor,Integer houseId){
        return houseService.getRooms(floor,houseId);
    }

    /**
      * @Description: 添加楼层
      * @Author zys
      * @Date 2020/6/21 0021 12:23
      * @Param [map]
     */
    @RequestMapping("addFloor")
    public Object addFloor(@RequestBody Map map){
        return houseService.addFloor(map);
    }

    /**
      * @Description: 添加维修
      * @Author zys
      * @Date 2020/6/23 0023 20:49
      * @Param [map]
     */
    @RequestMapping("addRepair")
    public Object addRepair(@RequestBody Map map){
        return houseService.addRepair(map);
    }

    /**
      * @Description: 查询床位
      * @Author zys
      * @Date 2020/6/21 0021 16:07
      * @Param [houseId, floorId, roomId]
     */
    @RequestMapping("beds")
    public Object listBeds(Integer houseId,Integer floorId, Integer roomId){
        return houseService.listBeds(houseId,floorId,roomId);
    }

    /**
      * @Description: 删除宿舍
      * @Author zys
      * @Date 2020/6/21 0021 17:14
      * @Param [houseId]
     */
    @RequestMapping("delete")
    public Object deleteByHouseId(Integer houseId){
        return houseService.deleteByHouseId(houseId);
    }
    /**
      * @Description: 修改房间使用状态
      * @Author zys
      * @Date 2020/6/21 0021 17:15
      * @Param [bedId]
     */
    @RequestMapping("changeStatus")
    public Object changeStatus(Integer id,Integer status){
        return houseService.changeStatus(id,status);
    }
}
