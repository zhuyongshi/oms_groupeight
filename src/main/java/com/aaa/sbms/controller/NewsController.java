package com.aaa.sbms.controller;

import com.aaa.sbms.service.NewsService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * fileName:NewsController
 * description:
 * author:zz
 * createTime:2019/11/26 14:19
 * version:1.0.0
 */
@RestController
@RequestMapping("news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    /**
     * 新闻列表
     * @return
     */
    @RequestMapping("list")
    public Object list(){
       /* Map map =new HashMap();
        map.put("userInfo","欢迎你"+
                ((Map)SecurityUtils.getSubject().getSession().getAttribute("userInfo")).get("REAL_NAME"));
        map.put("newsList",newsService.getList());*/
        return newsService.getList();
    }

    /**
     * 新闻分页
     * @param map
     * @return
     */
    @RequestMapping("page")
    public Object page(@RequestParam Map map){
        return newsService.getPage(map);
    }


    /**
     * 添加
     * @param map
     * @return
     */
    @PostMapping("add")
    public Object add(@RequestBody Map map) {
        return newsService.add(map);
    }

    /**
     * 修改
     * @param map
     * @return
     */
    @RequestMapping("update")
    public Object update(@RequestBody Map map) {
        return newsService.update(map);
    }

    /**
     * 根据编号删除
     * @param id
     * @return
     */
    @RequestMapping("delete")
    public Object delete(Integer id) {
        return newsService.delete(id);
    }
}
