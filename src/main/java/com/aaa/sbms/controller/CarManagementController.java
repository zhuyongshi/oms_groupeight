package com.aaa.sbms.controller;

import com.aaa.sbms.service.CarService;
        import com.aaa.sbms.util.SessionUtil;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestBody;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestParam;
        import org.springframework.web.bind.annotation.RestController;

        import javax.servlet.http.HttpSession;
        import java.util.Map;

/**
 * 车辆管理控制层
 */
@RestController
@RequestMapping("vehicle")
public class CarManagementController {
    @Autowired
    private CarService carService;

    /**
      * @Description: 获取可用车辆
      * @Author zys
      * @Date 2020/6/30 0030 21:43
      * @Param []
     */
    @RequestMapping("getUserCar")
    public Object getUserCar(){
       return carService.getUserCar();
    }

    /**
      * @Description:
      * @Author zys
      * @Date 2020/6/30 0030 21:57
      * @Param [map]
     */
    @RequestMapping("useCar")
    public Object useCar(@RequestBody Map map){
      return carService.toUseCar(map);
    }

    /**
     * 查询所有车辆信息
     * @param map
     * @return
     */
    @RequestMapping("carInfo")
    public Object selectAllCar(@RequestParam  Map map){
        return carService.selectAllCar(map);
    }

    /**
     * 根据id删除车辆
     * @param carid 车辆ID
     * @return 返回值
     */
    @RequestMapping("delete")
    public Object deleteCarId(Integer carid){
        System.out.println(carid);
        return carService.deleteCarId(carid);
    }

    /**
     * 添加
     * @param map
     * @return
     */
    @RequestMapping("add")
    public Object addCar(@RequestBody Map map){
        return carService.addCar(map);
    }

    /**
     * 更新车辆信息
     * @param map
     * @param session
     * @return
     */
    @RequestMapping("update")
    public Object updateCar(@RequestBody Map map, HttpSession session){
//        map.put("userName", SessionUtil.getUserName());
        map.put("userName", SessionUtil.getUserName());
        return carService.updataCar(map);
    }
}
