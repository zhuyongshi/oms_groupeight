package com.aaa.sbms.controller;

import com.aaa.sbms.service.CarmaintainService;
import com.aaa.sbms.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;
/**
  * @Description: 维修
  * @Author zys
  * @Date 2020/6/30 0030 21:28
  * @Param
 */
@RestController
@RequestMapping("maintain")
public class CarmaintainManagementController {
    @Autowired
    private CarmaintainService carmaintainService;

    /**
     * 查询所有
     * @param map
     * @return
     */
    @RequestMapping("list")
    public Object selectAllMain(@RequestParam Map map){
        return carmaintainService.selectAllMain(map);
    }

    /**
     * 删除
     * @param mainid
     * @return
     */
    @RequestMapping("delete")
    public Object deleteMainId(Integer mainid){
        return carmaintainService.deleteMainId(mainid);
    }

    /**
     * 添加
     * @param map
     * @return
     */
    @RequestMapping("add")
    public Object addMain(@RequestBody Map map){
        return carmaintainService.addMain(map);
    }

    /**
     * 更新
     * @param map
     * @param session
     * @return
     */
    @RequestMapping("update")
    public Object updateMain(@RequestBody Map map,HttpSession session){
        map.put("username", SessionUtil.getUserName());
        return carmaintainService.updateMain(map);
    }

    /**
      * @Description: 车辆报修
      * @Author zys
      * @Date 2020/7/1 0001 2:18
      * @Param [map]
     */
    @RequestMapping("addMaintain")
    public Object addMaintain(@RequestBody Map map){
        return carmaintainService.addMaintain(map);
    }

    /**
      * @Description: 维修
      * @Author zys
      * @Date 2020/7/1 0001 2:56
      * @Param [map]
     */
    @RequestMapping("toMaintain")
    public Object toMaintain(@RequestBody Map map){
        return carmaintainService.toMaintain(map);
    }
}
