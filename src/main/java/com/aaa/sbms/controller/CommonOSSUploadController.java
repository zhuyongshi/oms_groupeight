package com.aaa.sbms.controller;

import com.aaa.sbms.service.CommonOSSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * fileName:CommonOSSUploadController
 * description:
 * author:zz
 * createTime:2020/3/20 15:14
 * version:1.0.0
 */
@RestController
@RequestMapping("commonOSS")
public class CommonOSSUploadController {

    @Autowired
    private CommonOSSService commonOSSService;

    /**
     * 上传文件至阿里云 oss
     *
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/upload", method = {RequestMethod.POST}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> uploadOSS(@RequestParam(value = "file") MultipartFile file) throws Exception {
        Map result = commonOSSService.uploadOSS(file);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<>(result, headers, HttpStatus.CREATED);
    }
}
