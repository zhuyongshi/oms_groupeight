package com.aaa.sbms.controller;

import com.aaa.sbms.service.CoursePlanService;
import com.aaa.sbms.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 课程计划
 * Fong
 */
@RestController
@RequestMapping("coursePlan")
public class CoursePlanController {

    @Autowired
    private CoursePlanService coursePlanService;

    /**
     * 查询所有
     *
     * @param map
     * @return
     */
    @RequestMapping("getCoursePlanAll")
    public Object getCoursePlanAll(@RequestParam Map map) {
        return coursePlanService.getCoursePlanAll(map);
    }

    /**
     * 根据ID删除课程计划
     *
     * @param coursePlanId
     * @return
     */
    @RequestMapping("deleteCoursePlanById")
    public Object deleteCoursePlanById(Integer coursePlanId) {
            return coursePlanService.deleteCoursePlanById(coursePlanId);


    }

    /**
     * 添加课程计划
     *
     * @param map
     * @param session
     * @return
     */
    @RequestMapping("addCoursePlan")
    public Object addCoursePlan(@RequestBody Map map, HttpSession session) throws ParseException {
        //获得用户名称
        map.put("userName", SessionUtil.getUserName());
        //格式化日期
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startTime = simpleDateFormat.parse((String)map.get("cp_startTime"));
        Date endTime = simpleDateFormat.parse((String)map.get("cp_endTime"));
        //日期对比
        if (startTime.after(endTime)) {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
        return coursePlanService.addCoursePlan(map);
    }

    @RequestMapping("updateCoursePlan")
    public Object updateCoursePlan(@RequestBody Map map, HttpSession session) throws ParseException{
        //获得用户名称
        map.put("userName", SessionUtil.getUserName());
        //格式化日期
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startTime = simpleDateFormat.parse((String)map.get("cp_startTime"));
        Date endTime = simpleDateFormat.parse((String)map.get("cp_endTime"));
        //日期对比
        if (startTime.after(endTime)) {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
        return coursePlanService.updateCoursePlan(map);
    }

    @RequestMapping("getForm")
    public Object getFormDate(){
        return coursePlanService.getFormDate();
    }

}
