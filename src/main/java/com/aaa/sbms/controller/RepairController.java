package com.aaa.sbms.controller;

import com.aaa.sbms.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * className:RepairController
 * description:
 * author:zys
 * createTime:2020/6/23 0023 16:40
 */
@RestController
@RequestMapping("repair")
public class RepairController {

    @Autowired
    private RepairService repairService;

    /**
      * @Description: 获取修理信息
      * @Author zys
      * @Date 2020/6/23 0023 16:44
      * @Param [map]
     */
    @RequestMapping("page")
    public Object listRepair(@RequestParam Map map){
        return repairService.listRepair(map);
    }

    /**
      * @Description: 处理维修
      * @Author zys
      * @Date 2020/6/23 0023 17:21
      * @Param [map]
     */
    @RequestMapping("toHandle")
    public Object toHandle(@RequestBody Map map){
        return repairService.toHandle(map);
    }
}
