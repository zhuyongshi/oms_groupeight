package com.aaa.sbms.controller;

import com.aaa.sbms.service.QuitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * className:QuitController
 * description:
 * author:zys
 * createTime:2020/6/24 0024 15:13
 */
@RestController
@RequestMapping("quit")
public class QuitController {

    @Autowired
    private QuitService quitService;

    /**
      * @Description: 获取离职列表
      * @Author zys
      * @Date 2020/6/24 0024 15:16
      * @Param [map]
     */
    @RequestMapping("page")
    public Object listQuit(@RequestParam Map map){
      return quitService.listQuit(map);
    }

    /**
      * @Description: 获取待审核离职信息
      * @Author zys
      * @Date 2020/6/24 0024 17:00
      * @Param [map]
     */
    @RequestMapping("record")
    public Object listsRecord(@RequestParam Map map){
        return  quitService.listsRecord(map);
    }

    /**
      * @Description: 通过审批
      * @Author zys
      * @Date 2020/6/24 0024 17:26
      * @Param [map]
     */
    @RequestMapping("pass")
    public Object updateCheckState(@RequestBody Map map){
        return quitService.updateCheckState(map);
    }
    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/24 0024 17:32
      * @Param [map]
     */
    @RequestMapping("unPass")
    public Object changeCheckState(@RequestBody Map map){
        return quitService.changeCheckState(map);
    }

    /**
      * @Description: 提交离职申请
      * @Author zys
      * @Date 2020/6/29 0029 13:27
      * @Param [map]
     */
    @RequestMapping("toLeave")
    public Object toLeave(@RequestBody Map map){
        return quitService.toLeave(map);
    }

}
