package com.aaa.sbms.controller;


import com.aaa.sbms.service.CloudService;
import com.aaa.sbms.service.CommonService;
import com.aaa.sbms.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;

/**
 * 网盘
 *
 * @author 10744
 */
@RestController
@RequestMapping("html/cloud")
public class CloudController {

    @Autowired
    private CloudService cloudService;

    @Autowired
    private CommonService commonService;

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @RequestMapping("upload")
    public Object upload(@RequestParam MultipartFile file) {
        long size = file.getSize();
        if (size < 5242880) {
            int userId = SessionUtil.getUserId();
            return cloudService.upload(userId, file);
        }
        return 500;
    }

    /**
     * 文件列表
     *
     * @param session
     * @return
     */
    @RequestMapping("getAll")
    public Object getFileList(HttpSession session) {
        int userId = SessionUtil.getUserId();
        return cloudService.getFileList(userId);
    }

    /**
     * 文件下载
     *
     * @param response
     * @param fileId
     */
    @RequestMapping("download")
    public void download(HttpServletResponse response, Integer fileId) {
        Map file = cloudService.getFileByid(fileId);
        String fileName = (String) file.get("path");
        //设置字符集
        response.setCharacterEncoding("UTF-8");
        //设置下载内容类型
        response.setContentType("application/octet-stream;charset=utf-8");
        try {
            //解决下载时文件名乱码：URLEncoder.encode(file.get("f_name")+"", "utf-8")
            response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(file.get("f_name") + "", "utf-8"));
            OutputStream output = response.getOutputStream();
            commonService.downloadFile(output, fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 重命名文件夹
     * @param map
     * @return
     */
    @RequestMapping("update")
    public Object updateFileName(@RequestBody Map map) {
        return cloudService.updateFileName(map);
    }

    /**
     * 删除文件（移到回收站）
     * @param fileId
     * @return
     */
    @RequestMapping("delete")
    public Object delete(Integer fileId){
        System.out.println(fileId);
        return cloudService.deleteFile(fileId);
    }
}
