package com.aaa.sbms.controller;

import com.aaa.sbms.service.CheckInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * className:CheckInController
 * description:
 * author:zys
 * createTime:2020/6/22 0022 14:55
 */
@RestController
@RequestMapping("checkIn")
public class CheckInController {

    @Autowired
    private CheckInService checkInService;

    /**
      * @Description: 获取入住员工信息
      * @Author zys
      * @Date 2020/6/22 0022 15:16
      * @Param [map]
     */
    @RequestMapping("page")
    public Object listCheckIn(@RequestParam Map map){
        return checkInService.listCheckIn(map);
    }

    /**
      * @Description: 修改入住意向
      * @Author zys
      * @Date 2020/6/22 0022 16:01
      * @Param [userId]
     */
    @RequestMapping("changeIsLock")
    public Object changeIsLock(Integer userId,Integer isLock){
        return checkInService.changeIsLock(userId,isLock);
    }

    /**
      * @Description: 根据性别获取宿舍地址
      * @Author zys
      * @Date 2020/6/22 0022 16:51
      * @Param [sex]
     */
    @RequestMapping("listHouses")
    public Object listHouses(Integer sex){
        return checkInService.listHouses(sex);
    }

    /**
      * @Description: 根据宿舍获取楼层
      * @Author zys
      * @Date 2020/6/22 0022 17:22
      * @Param [houseId]
     */
    @RequestMapping("getFloors")
    public Object getFloors(Integer houseId){
        return checkInService.getFloors(houseId);
    }

    /**
      * @Description: 根据楼层获取房间
      * @Author zys
      * @Date 2020/6/22 0022 17:48
      * @Param [houseId, floor]
     */
    @RequestMapping("getRooms")
    public Object getRooms(@RequestParam Map map){
        return checkInService.getRooms(map);
    }

    /**
      * @Description: 获取床位
      * @Author zys
      * @Date 2020/6/22 0022 22:09
      * @Param [map]
     */
    @RequestMapping("getBeds")
    public Object getBeds(@RequestParam Map map){
        return checkInService.getBeds(map);
    }

    /**
      * @Description: 添加入住信息
      * @Author zys
      * @Date 2020/6/22 0022 22:21
      * @Param [map]
     */
    @RequestMapping("checkinInfo")
    public Object checkInInfo(@RequestBody Map map){
        return checkInService.checkInInfo(map);
    }
}
