package com.aaa.sbms.controller;

import com.aaa.sbms.util.AppConfig;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    AppConfig appConfig;

    @RequestMapping("/editor")
    @ResponseBody
    public Object editor(@RequestParam("file") MultipartFile file) {
        String fileName = "";
        if (!file.isEmpty()) {
            //返回的是字节长度,1M=1024k=1048576字节 也就是if(fileSize<5*1048576)
            if (file.getSize() > (1048576 * 5)) {
                return "文件太大，请上传小于5MB的";
            }
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            if (StringUtils.isBlank(suffix)) {
                return "上传文件没有后缀，无法识别";
            }

            fileName = System.currentTimeMillis() + suffix;
            String saveFileName = appConfig.getFilepath() + "/article/" + fileName;
            System.out.println(saveFileName);
            File dest = new File(saveFileName);
            System.out.println(dest.getParentFile().getPath());
            //判断文件父目录是否存在
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdir();
            }
            try {
                //保存文件
                file.transferTo(dest);
            } catch (Exception e) {
                e.printStackTrace();
                return new WangEditorResponse("1", "上传失败" + e.getMessage());
            }
        } else {
            return new WangEditorResponse("1", "上传出错");
        }
        String imgUrl = appConfig.getUrlpath() + "article/" + fileName;
        return new WangEditorResponse("0", imgUrl);
    }

    @Data
    private class WangEditorResponse {
        String errno;
        List<String> data;

        public WangEditorResponse(String errno, List<String> data) {
            this.errno = errno;
            this.data = data;
        }

        public WangEditorResponse(String errno, String data) {
            this.errno = errno;
            this.data = new ArrayList<>();
            this.data.add(data);
        }
    }
}
