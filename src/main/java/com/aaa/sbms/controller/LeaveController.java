package com.aaa.sbms.controller;

import com.aaa.sbms.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * className:LeaveController
 * description:
 * author:zys
 * createTime:2020/6/29 0029 20:19
 */
@RestController
@RequestMapping("firstRecord")
public class LeaveController {

    @Autowired
    private LeaveService leaveService;

    /**
      * @Description: 获取人事审核信息
      * @Author zys
      * @Date 2020/6/29 0029 20:26
      * @Param [map]
     */
    @RequestMapping("page")
    public Object listFirstRecord(@RequestParam Map map){
        return leaveService.listFirstRecord(map);
    }

    /**
      * @Description: 获取领导审核信息
      * @Author zys
      * @Date 2020/6/29 0029 21:38
      * @Param [map]
     */
    @RequestMapping("list")
    public Object listTwoRecord(@RequestParam Map map){
        return leaveService.listTwoRecord(map);
    }

    /**
      * @Description: 批准
      * @Author zys
      * @Date 2020/6/29 0029 20:58
      * @Param [map]
     */
    @RequestMapping("pass")
    public Object toPass(@RequestBody Map map){
        return leaveService.toPass(map);
    }

    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/29 0029 21:04
      * @Param [map]
     */
    @RequestMapping("unPass")
    public Object toUnPass(@RequestBody Map map){
        return leaveService.toUnPass(map);
    }

    /**
     * @Description: 获取审核记录信息
     * @Author zys
     * @Date 2020/6/29 0029 20:26
     * @Param [map]
     */
    @RequestMapping("hisPage")
    public Object listHistoryList(@RequestParam Map map){
        return leaveService.listHistoryList(map);
    }

    /**
     * @Description: 二次批准
     * @Author zys
     * @Date 2020/6/29 0029 20:58
     * @Param [map]
     */
    @RequestMapping("twoPass")
    public Object toTowPass(@RequestBody Map map){
        return leaveService.toTowPass(map);
    }

    /**
     * @Description: 二次驳回
     * @Author zys
     * @Date 2020/6/29 0029 21:04
     * @Param [map]
     */
    @RequestMapping("towUnPass")
    public Object toTowUnPass(@RequestBody Map map){
        return leaveService.toTowUnPass(map);
    }

    /**
      * @Description: 请假
      * @Author zys
      * @Date 2020/6/30 0030 16:25
      * @Param [map]
     */
    @RequestMapping("addLeave")
    public Object toAddLeave(@RequestBody Map map){
        return leaveService.toAddLeave(map);
    }

    /**
      * @Description: 查询请假结果
      * @Author zys
      * @Date 2020/6/30 0030 17:49
      * @Param []
     */
    @RequestMapping("leaveResult")
    public Object checkResult(){
        return leaveService.checkResult();
    }
}
