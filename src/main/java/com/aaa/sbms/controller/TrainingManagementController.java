package com.aaa.sbms.controller;

import com.aaa.sbms.entity.Teacher;
import com.aaa.sbms.service.TeacherService;
import com.aaa.sbms.util.FileUtil;
import com.aaa.sbms.util.SessionUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * 培训管理控制层
 * Fong
 */
@RestController
@RequestMapping("training")
public class TrainingManagementController {
    @Autowired
    private TeacherService teacherService;

    /**
     * 查询所有教师信息
     * @param map
     * @return
     */
    @RequestMapping("teacherinfo")
    public Object selectAllTeacher(@RequestParam Map map){
        return teacherService.selectAllTeacher(map);
    }

    /**
     * 根据教师ID删除信息
     * @param teacherId
     * @return
     */
    @RequestMapping("teacherdel")
    public Object deleteTeacherById(Integer teacherId){
        System.out.println(teacherId);
        return teacherService.deleteTeacherById(teacherId);
    }

    /**
     * 添加教师信息
     * @param map
     * @param httpSession
     * @return
     */
    @RequestMapping("teacheradd")
    public Object addTeacher(@RequestBody Map map, HttpSession httpSession){
        map.put("userName", SessionUtil.getUserName());
        return teacherService.addTeacher(map);
    }

    /**
     * 编辑教师信息
     * @param map
     * @param httpSession
     * @return
     */
    @RequestMapping("teacherupdate")
    public Object updateTeacher(@RequestBody Map map,HttpSession httpSession){
        map.put("userName",SessionUtil.getUserName());
        return teacherService.updateTeacher(map);
    }

    /**
     * 批量删除
     * @param list
     * @return
     */
    @RequestMapping("teacherdeldata")
    public Object deleteDatas(@RequestBody List<Integer> list){
        System.out.println(list);
        return teacherService.deleteDatas(list);
    }

    /**
     * 得到教师的名字和ID
     * @return
     */
    @RequestMapping("getName")
    public Object getName(){
        return teacherService.getName();
    }

    /**
     * 下载方法
     * @param response
     * @throws Exception
     */
    @RequestMapping("write")
    public void writeExcel(HttpServletResponse response) throws Exception {
        //文件名
        String fileName = "TeacherInfo";
        ServletOutputStream download = FileUtil.Download(response, fileName);
        ExcelWriterBuilder write = EasyExcel.write(download, Teacher.class);
        List<Teacher> list = teacherService.getAllTeacher();
        write.sheet().doWrite(list);
    }
}
