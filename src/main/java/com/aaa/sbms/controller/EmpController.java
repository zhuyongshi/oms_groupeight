package com.aaa.sbms.controller;

import com.aaa.sbms.entity.Emp;
import com.aaa.sbms.service.UserService;
import com.aaa.sbms.util.FileUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * className:EmpController
 * description:
 * author:zys
 * createTime:2020/6/11 0011 13:13
 */
@RestController
@RequestMapping("emp")
public class EmpController {

    @Autowired
    private UserService userService;

  /**
    * @Description: 员工列表
    * @Author zys
    * @Date 2020/6/26 0026 12:39
    * @Param [map]
   */
  @RequestMapping("page")
    public Object getEmpList(@RequestParam Map map){
        return userService.getEmpList(map);
    }

    /**
      * @Description: 更新员工信息
      * @Author zys
      * @Date 2020/6/26 0026 12:39
      * @Param [map]
     */
    @RequestMapping("update")
    public Object updateEmp(@RequestBody Map map){
      return userService.update(map);
    }

    /**
      * @Description: 获取直系领导
      * @Author zys
      * @Date 2020/6/26 0026 12:39
      * @Param [deptId]
     */
    @RequestMapping("getLeader")
    public Object getLeader(Integer deptId){
        return userService.getLeader(deptId);
    }

   /**
     * @Description: 添加员工信息
     * @Author zys
     * @Date 2020/6/26 0026 12:38
     * @Param [map]
    */
    @RequestMapping("add")
    public Object addEmp(@RequestBody Map map){
        return userService.add(map);
    }

    /**
      * @Description: 删除用户
      * @Author zys
      * @Date 2020/6/26 0026 12:38
      * @Param [userId]
     */
    @RequestMapping("delete")
    public Object delete(Integer userId){
        return userService.delete(userId);
    }

    @RequestMapping("getPerson")
    public Object getPerson(){
        return userService.getPerson();
    }

    /**
      * @Description: 修改头像
      * @Author zys
      * @Date 2020/6/26 0026 12:38
      * @Param [imageUrl]
     */
    @RequestMapping("updateAvatar")
    public Object updateAvatar(String imageUrl){
        return userService.updateAvatar(imageUrl);
    }

    /**
     * @Description: 更新个人信息
     * @Author zys
     * @Date 2020/6/26 0026 12:38
     * @Param [map]
    */
    @RequestMapping("updatePerson")
    public Object updatePerson(@RequestBody Map map){
        return userService.updatePerson(map);
    }

    /**
     * 下载方法
     * @param response
     * @throws Exception
     */
    @RequestMapping("write")
    public void writeExcel(HttpServletResponse response) throws Exception {
        //文件名
        String fileName = "empInfo";
        ServletOutputStream download = FileUtil.Download(response, fileName);
        ExcelWriterBuilder write = EasyExcel.write(download, Emp.class);
        List<Emp> list = userService.getAllEmp();
        write.sheet().doWrite(list);
    }
}
