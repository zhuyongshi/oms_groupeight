package com.aaa.sbms.controller;

import com.aaa.sbms.service.AutideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
/**
  * @Description: 审核
  * @Author zys
  * @Date 2020/6/30 0030 21:28
  * @Param
 */
@RestController
@RequestMapping("audit")
public class AuditManagementController {
    @Autowired
    private AutideService autideService;

    /**
     * 查询所有的审核信息
     * @param map
     * @return
     */
    @RequestMapping("all")
    public Object selectAllAutid(@RequestParam Map map){
        return autideService.selectAllAutid(map);
    }


    /**
     * 审核
     * @param map
     * @return
     */
    @RequestMapping("update")
    public Object updateAutidId(@RequestBody Map map){
        return autideService.updateAutidId(map);
    }

    /**
     * 更新
     * @param map
     * @return
     */
    @RequestMapping("unPass")
    public Object unPass(@RequestBody Map map){
        return autideService.unPass(map);
    }

    /**
      * @Description: 获取当前用户借用的车辆
      * @Author zys
      * @Date 2020/6/30 0030 23:36
      * @Param []
     */
    @RequestMapping("getUserCarByUserId")
    public Object getUserCarByUserId(){
        return autideService.getUserCarByUserId();
    }

    /**
      * @Description: 归还车辆
      * @Author zys
      * @Date 2020/7/1 0001 0:25
      * @Param [map]
     */
    @RequestMapping("updaterState")
    public Object updaterState(@RequestBody Map map){
        return autideService.updaterState(map);
    }

}
