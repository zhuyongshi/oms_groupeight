package com.aaa.sbms.controller;

import com.aaa.sbms.dao.ResultDao;
import com.aaa.sbms.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 考核结果
 * @author 10744
 */
@RestController
@RequestMapping("result")
public class ResultController {

    @Autowired
    private ResultService resultService;

    @RequestMapping("getAll")
    public Object getAll(@RequestParam Map map){
        return resultService.getAll(map);
    }

    @RequestMapping("update")
    public Object update(@RequestBody Map map){
        System.out.println(map);
        return resultService.update(map);
    }

    @RequestMapping("getForm")
    public Object getForm(){
        return resultService.getForm();
    }
}
