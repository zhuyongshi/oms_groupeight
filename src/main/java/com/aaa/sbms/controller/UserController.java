package com.aaa.sbms.controller;

import com.aaa.sbms.service.UserService;
import com.aaa.sbms.util.SessionUtil;
import org.apache.catalina.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * fileName:UserController
 * description:
 * author:zz
 * createTime:2019/11/26 16:45
 * version:1.0.0
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 获取登录session
     * @param session
     * @return
     */
    @RequestMapping("getLoginSession")
    public Object getSession(HttpSession session){
         return session.getAttribute("userInfo")==null?null:(Map)session.getAttribute("userInfo");
    }
    /**
     * 登录方法
     * @param tel
     * @param passWord
     * @return
     */
    @RequestMapping("login")
    public Object login(String tel, String passWord, HttpServletRequest request) {
       /* try {
            Map map = new HashMap();
            map.put("status","0");//判断用户是否有效  如果传入数字写为字符串
            map.put("userName",userName);
            map.put("passWord",passWord);
            map.put("pageNo",1);
            map.put("pageSize",1000);
            Map resultMap = userService.getList(map);
            int total= Integer.valueOf(resultMap.get("total")+"");
            if(total>0){
                //更新登录时间和登录IP 记录最后
                map.put("loginIp",request.getRemoteAddr());
               userService.updateLoginInfoByUserName(map);
                //保存session
                session.setAttribute("userInfo",((List<Map>)resultMap.get("page")).get(0));
                //工具类保存session，方便调用
                SessionUtil.setSession(session);
                return 1;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }*/
        //收集用户输入
        AuthenticationToken token = new UsernamePasswordToken(tel, passWord);
        //获取登录对象
        Subject crrentUser = SecurityUtils.getSubject();
        //判断是否已经人认证
        if (!crrentUser.isAuthenticated()){
            try {
                //提交数据  交给SecurityManager进行认证   登录
                crrentUser.login(token);
                //更新登录时间和登录IP
                Map map  = new HashMap();
               // map.put("loginIp",request.getRemoteAddr());
                //map.put("userName",userName);
               // userService.updateLoginInfoByUserName(map);
                //记录session
                Session session = crrentUser.getSession();
                session.setAttribute("userInfo", (Map) crrentUser.getPrincipal());
                //封装方法中设置session
                SessionUtil.setSession(session);
            } catch (AuthenticationException e) {
                e.printStackTrace();
                return 0;
            }
        }
        return 1;
    }

    /**
      * @Description: 更新密码
      * @Author zys
      * @Date 2020/6/17 0017 16:07
      * @Param [map]
      * @return java.lang.Object
     */
    @RequestMapping("updatePwd")
    public Object updatePwd(String password){
        return userService.updatePwd(password);
    }
    /**
     * 根据条件查询
     * @param map
     * @return
     */
    @RequestMapping("page")
   public  Object list(@RequestParam Map map){
       return userService.getList(map);
   }

    /***
     * 添加
     * @param map
     * @return
     */
    @RequestMapping("add")
    public  Object add(@RequestBody  Map map){
        return userService.add(map);
    }

    /**
     * 更新
     * @param map
     * @return
     */
    @RequestMapping("update")
    public  Object update(@RequestBody Map map){
        return userService.update(map);
    }

    /**
     * 删除
     * @param userId
     * @return
     */
    @RequestMapping("delete")
    public  Object delete(Integer userId){
        return userService.delete(userId);
    }

    /**
     *查询用户id对应的角色id集合
     * @param userId
     * @return
     */
    @RequestMapping("listRoleIdsByUserId")
    public Object listRoleIdsByUserId(Integer userId){
        return userService.listRoleIdsByUserId(userId);
    }

    /**
     * 添加用户角色关联
     * @param userId
     * @param roleIds
     * @return
     */
    @RequestMapping("addUserAndRole")
    public Object addUserAndRole(Integer userId,String roleIds){
         return userService.addUserAndRole(userId,roleIds);
    }

    /**
      * @Description:删除用户角色
      * @Author zys
      * @Date 2020/6/17 0017 19:07
      * @Param [user_id]
      * @return java.lang.Object
     */
    @RequestMapping("delRole")
    public Object delRole(Integer user_id){
        return userService.delRole(user_id);
    }

}
