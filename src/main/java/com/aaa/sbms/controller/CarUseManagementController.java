package com.aaa.sbms.controller;

import com.aaa.sbms.service.CarUserService;
import com.aaa.sbms.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;
/**
  * @Description: 车辆使用
  * @Author zys
  * @Date 2020/6/30 0030 21:28
  * @Param
 */
@RestController
@RequestMapping("use")
public class CarUseManagementController {
    @Autowired
    private CarUserService carUserService;
    /**
     * 查询所有
     * @param map
     * @return
     */
    @RequestMapping("list")
    public Object selectAllUse(@RequestParam Map map){
        return carUserService.selectAllUse(map);
    }

    /**
      * @Description: 维修记录
      * @Author zys
      * @Date 2020/7/1 0001 4:37
      * @Param [map]
     */
    @RequestMapping("listRepair")
    public Object listRepair(@RequestParam Map map){
        return carUserService.listRepair(map);
    }
}
