package com.aaa.sbms.controller;

import com.aaa.sbms.service.CheckOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * className:CheckOutController
 * description:
 * author:zys
 * createTime:2020/6/23 0023 14:35
 */
@RestController()
@RequestMapping("checkOut")
public class CheckOutController {

    @Autowired
    private CheckOutService checkOutService;

    /**
      * @Description: 获取退宿列表
      * @Author zys
      * @Date 2020/6/23 0023 14:50
      * @Param [map]
     */
    @RequestMapping("page")
    public Object listCheckOut(@RequestParam Map map){
        return checkOutService.listCheckOut(map);
    }

    /**
      * @Description: 办理退宿
      * @Author zys
      * @Date 2020/6/23 0023 15:15
      * @Param [id]
     */
    @RequestMapping("toCheckOut")
    public Object toCheckOut(@RequestBody Map map){
        return checkOutService.toCheckOut(map);
    }
}
