package com.aaa.sbms.controller;

import com.aaa.sbms.service.RecycleService;
import com.aaa.sbms.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * 回收站
 * @author 10744
 */
@RestController
@RequestMapping("recycle")
public class RecycleController {

    @Autowired
    private RecycleService recycleService;

    /**
     * 回收站文件列表
     * @param
     * @return
     */
    @RequestMapping("getAll")
    public Object getFileList(HttpSession session) {
        int userId = SessionUtil.getUserId();
        return recycleService.getFileList(userId);
    }

    /**
     * 彻底删除文件
     * @param fileId
     * @return
     */
    @RequestMapping("delete")
    public Object deleteFile(Integer fileId) {
        return recycleService.deleteFile(fileId);
    }

    /**
     * 还原文件
     * @param fileId
     * @return
     */
    @RequestMapping("rollback")
    public Object rollback(Integer fileId) {
        return recycleService.rollback(fileId);
    }

}
