package com.aaa.sbms.controller;

import com.aaa.sbms.service.CourseService;
import com.aaa.sbms.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * 课程管理控制层
 * Fong
 */
@RestController
@RequestMapping("course")
public class CourseController {
    @Autowired
    private CourseService courseService;

    /**
     * 查询所有课程
     * @param map
     * @return
     */
    @RequestMapping("getCourseAll")
    public Object getCourseAll(@RequestParam Map map){
        return courseService.getCourseAll(map);
    }

    /**
     * 根据ID删除课程
     * @param courseId
     * @return
     */
    @RequestMapping("deleteCourseById")
    public Object deleteCourseById(Integer courseId){
        return courseService.deleteCourseById(courseId);
    }

    /**
     * 添加课程
     * @param map
     * @param session
     * @return
     */
    @RequestMapping("addCourse")
    public Object addCourse(@RequestBody Map map, HttpSession session){
        map.put("userName", SessionUtil.getUserName());
        return courseService.addCourse(map);
    }

    /**
     * 编辑课程
     * @param map
     * @param session
     * @return
     */
    @RequestMapping("updataCourse")
    public Object updataCourse(@RequestBody Map map,HttpSession session){
        map.put("userName",SessionUtil.getUserName());
        return courseService.updataCourse(map);
    }

    /**
     * 批量删除
     * @param list
     * @return
     */
    @RequestMapping("deleteCourses")
    public Object deleteCourses(@RequestBody List<Integer> list){
        return courseService.deleteCourses(list);
    }
}
