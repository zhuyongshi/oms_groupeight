package com.aaa.sbms.service;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface RecycleService {

    /**
     * 查询文件
     * @param userId
     * @return
     */
    List<Map> getFileList(int userId);

    /**
     * 彻底删除
     * @param fileId
     * @return
     */
    int deleteFile(Integer fileId);

    /**
     * 还原文件到我的文件
     * @param fileId
     * @return
     */
    int rollback(Integer fileId);
}
