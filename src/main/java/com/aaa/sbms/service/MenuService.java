package com.aaa.sbms.service;

import com.aaa.sbms.entity.TreeNode;

import java.util.List;
import java.util.Map;

/**
 * fileName:MenuService
 * description:
 * author:zz
 * createTime:2019/11/28 16:10
 * version:1.0.0
 */
public interface MenuService {

    /**
     * 拼装树形数据
     * @return
     */
    List<TreeNode> getTreeData();

    /***
     * 添加
     * @param map
     * @return
     */
    int add(Map map);

    /**
     * 更新
     * @param map
     * @return
     */
    int update(Map map);

    /**
     * 删除
     * @param menuId
     * @return
     */
    int delete(int menuId);

    /**
     * 根据登录用户id查询该用户对象的所有权限菜单#{userId}
     * @param userId
     * @return
     */
    List<TreeNode> listMenuByUserId(int userId);
 }
