package com.aaa.sbms.service;

import java.util.List;
import java.util.Map;

public interface AutideService {
    /**
     * 查询所有
     * @param map
     * @return
     */
    Map selectAllAutid(Map map);


    /**
     * 更新
     * @param map
     * @return
     */
    int updateAutidId(Map map);


    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/30 0030 23:08
      * @Param [map]
     */
    int unPass(Map map);

    /**
      * @Description: 获取当前用户借用的车辆
      * @Author zys
      * @Date 2020/6/30 0030 23:37
      * @Param []
     */
    Map getUserCarByUserId();

    /**
      * @Description: 归还车辆
      * @Author zys
      * @Date 2020/7/1 0001 0:26
      * @Param [map]
     */
    int updaterState(Map map);
}
