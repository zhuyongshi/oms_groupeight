package com.aaa.sbms.service;


import java.util.List;
import java.util.Map;

public interface HouseService {
    /**
      * @Description:
      * @Author zys
      * @Date 2020/6/19 0019 13:34
      * @Param [map]
      * @return java.util.Map
     */
    Map listHouse(Map map);

    /**
      * @Description: 添加宿舍
      * @Author zys
      * @Date 2020/6/19 0019 16:43
      * @Param [map]
     */
    int addHouse(Map map);
    /**
      * @Description:更新宿舍
      * @Author zys
      * @Date 2020/6/19 0019 20:35
      * @Param [map]
     */
    int updateHouse(Map map);

    /**
      * @Description: 获取房间号
      * @Author zys
      * @Date 2020/6/20 0020 10:52
      * @Param [floor, houseId]
     */
    List<Map> getRooms(Integer floor, Integer houseId);

    /**
      * @Description: 添加楼层
      * @Author zys
      * @Date 2020/6/20 0020 15:19
      * @Param [map]
     */
    int addFloor(Map map);

    /**
      * @Description: 获取床位号
      * @Author zys
      * @Date 2020/6/21 0021 12:57
      * @Param [houseId, floorId, roomId]
     */
    List<Map> listBeds(Integer houseId, Integer floorId, Integer roomId);

    /**
      * @Description: 删除楼层房间
      * @Author zys
      * @Date 2020/6/21 0021 16:12
      * @Param [houseId]
     */
    int deleteByHouseId(Integer houseId);

    /**
      * @Description: 修改床位状态
      * @Author zys
      * @Date 2020/6/21 0021 17:29
      * @Param [id]
     */
    int changeStatus(Integer id,Integer stauts);

    /**
      * @Description: 添加维修
      * @Author zys
      * @Date 2020/6/23 0023 20:49
      * @Param [map]
     */
    int addRepair(Map map);
}
