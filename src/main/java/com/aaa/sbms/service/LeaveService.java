package com.aaa.sbms.service;

import java.util.List;
import java.util.Map;

public interface LeaveService {


    /**
      * @Description: 获取人事审核信息
      * @Author zys
      * @Date 2020/6/29 0029 20:27
      * @Param [map]
     */
    Map listFirstRecord(Map map);

    /**
      * @Description: 批准
      * @Author zys
      * @Date 2020/6/29 0029 20:58
      * @Param [map]
     */
    int toPass(Map map);

    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/29 0029 21:05
      * @Param [map]
     */
    int toUnPass(Map map);

    /**
      * @Description: 获取审核记录信息
      * @Author zys
      * @Date 2020/6/29 0029 21:20
      * @Param [map]
     */
    Map listHistoryList(Map map);

    /**
      * @Description: 获取领导审核信息
      * @Author zys
      * @Date 2020/6/29 0029 21:39
      * @Param [map]
     */
    Map listTwoRecord(Map map);

    /**
      * @Description: 二次批准
      * @Author zys
      * @Date 2020/6/29 0029 21:43
      * @Param [map]
     */
    int toTowPass(Map map);

    /**
      * @Description: 二次驳回
      * @Author zys
      * @Date 2020/6/29 0029 21:47
      * @Param [map]
     */
    int toTowUnPass(Map map);

    /**
      * @Description: 请假
      * @Author zys
      * @Date 2020/6/30 0030 16:43
      * @Param [map]
     */
    int toAddLeave(Map map);

    /**
      * @Description:
      * @Author zys
      * @Date 2020/6/30 0030 17:49
      * @Param []
     */
    String checkResult();

}
