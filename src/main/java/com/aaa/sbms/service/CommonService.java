package com.aaa.sbms.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.OutputStream;
import java.util.Map;

public interface CommonService {

    Map uploadFile(MultipartFile file,String savePath);

    Boolean downloadFile(OutputStream outputStream, String fileName);
}
