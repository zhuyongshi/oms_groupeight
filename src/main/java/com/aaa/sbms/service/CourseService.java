package com.aaa.sbms.service;

import java.util.List;
import java.util.Map;

/**
 * 课程管理
 * Fong
 */
public interface CourseService {
    Map getCourseAll(Map map);

    int deleteCourseById(int courseId);

    int addCourse(Map map);

    int updataCourse(Map map);

    int deleteCourses(List<Integer> list);
}
