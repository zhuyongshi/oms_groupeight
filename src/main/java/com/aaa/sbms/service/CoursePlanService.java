package com.aaa.sbms.service;

import java.util.Map;

/**
 * 课程计划
 * Fong
 */
public interface CoursePlanService {
    Map getCoursePlanAll(Map map);

    int deleteCoursePlanById(int coursePlanId);

    int addCoursePlan(Map map);

    Map getFormDate();

    int updateCoursePlan(Map map);
}
