package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.CheckInDao;
import com.aaa.sbms.service.CheckInService;
import com.aaa.sbms.util.PageUtil;
import com.aaa.sbms.util.SessionUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * className:CheckInServiceImpl
 * description:
 * author:zys
 * createTime:2020/6/22 0022 14:57
 */
@Service
public class CheckInServiceImpl implements CheckInService {

    @Autowired
    private CheckInDao checkInDao;


    /**
      * @Description: 获取员工入住信息
      * @Author zys
      * @Date 2020/6/22 0022 15:29
      * @Param [map]
     */
    @Override
    public Map listCheckIn(Map map) {
        PageUtil.list(map);
        PageInfo pageInfo=new PageInfo(checkInDao.listCheckIn(map));
        Map checkMap=new HashMap();
        checkMap.put("page",pageInfo.getList());
        checkMap.put("total",pageInfo.getTotal());
        return checkMap;
    }

    /**
      * @Description:修改入住意向
      * @Author zys
      * @Date 2020/6/22 0022 16:03
      * @Param [userId]
     */
    @Override
    public int changeIsLock(Integer userId,Integer checkStatu) {
        //获取入住状态
        Integer isLock;
        if (checkStatu==0){
            isLock=1;
        }else {
            isLock=0;
        }
        return checkInDao.changeIsLock(userId,isLock);
    }

    /**
      * @Description: 根据性别获取宿舍地址
      * @Author zys
      * @Date 2020/6/22 0022 16:53
      * @Param [sex]
     */
    @Override
    public List<Map> listHouses(Integer sex) {
        List<Map> mapList = checkInDao.listHouses(sex);
        for (Map map : mapList) {
            String address =(String) map.get("address");
            String addr = address.replace(",", "");
            map.put("addr",addr);
        }
        return mapList;
    }

    /**
      * @Description: 根据宿舍获取楼层
      * @Author zys
      * @Date 2020/6/22 0022 17:22
      * @Param [houseId]
     */
    @Override
    public List<Map> getFloors(Integer houseId) {
        return checkInDao.getFloors(houseId);
    }

    /**
      * @Description: 根据楼层获取房间
      * @Author zys
      * @Date 2020/6/22 0022 17:49
      * @Param [houseId, floor]
     */
    @Override
    public List<Map> getRooms(Map map) {
        return checkInDao.getRooms(map);
    }

    /**
      * @Description: 根据房间号获取床位号
      * @Author zys
      * @Date 2020/6/22 0022 22:10
      * @Param [map]
     */
    @Override
    public List<Map> getBeds(Map map) {
        return checkInDao.getBeds(map);
    }

    /**
      * @Description: 添加入住信息
      * @Author zys
      * @Date 2020/6/22 0022 22:22
      * @Param [map]
     */
    @Transactional
    @Override
    public int checkInInfo(Map map) {
        //获取当前操作人的工号
        String workId = (String) SessionUtil.getUserInfo().get("workId");
        map.put("InsertWorkId",workId);
        Map bedA =(Map) map.get("bedA");
        //获取床位Id
        Integer bedId =(Integer) bedA.get("bedId");
        Integer bId =(Integer) bedA.get("bId");
        map.put("bedId",bedId);
        map.put("bId",bId);
        int count = checkInDao.checkInInfo(map);
        //修改床位床位状态
        int i = checkInDao.changeBedStatus(map);
        if (i>0){
            //查看当前房间是否还有空余床位
            List<Map> beds = checkInDao.getBeds(map);
            //如果没有
            if (beds.size()==0){
                //修改房价使用状态
                checkInDao.changeRoomsStatus(map);
            }
        }
        //修改用户入住状态
        Integer userId =(Integer) map.get("user_id");
        checkInDao.changeUserCheckinStatus(userId);
        return count;
    }
}