package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.CarmaintainDao;
import com.aaa.sbms.service.CarmaintainService;
import com.aaa.sbms.util.PageUtil;
import com.aaa.sbms.util.SessionUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CarmaintainServiceImpl implements CarmaintainService {
    @Autowired(required = false)
    private CarmaintainDao carmaintainDao;

    /**
     * 查询所有
     * @param map
     * @return
     */
    @Override
    public Map selectAllMain(Map map) {
        PageUtil.list(map);
        PageInfo<Map> pageInfo = new PageInfo<Map>(carmaintainDao.selectAllList(map));
        List<Map> car = carmaintainDao.getCar();
        Map rmap = new HashMap();
        rmap.put("car",car);
        rmap.put("total",pageInfo.getTotal());
        rmap.put("page",pageInfo.getList());
        return rmap;
    }

    /**
     * 删除
     * @param mainid
     * @return
     */
    @Override
    public int deleteMainId(Integer mainid) {
        return carmaintainDao.deleteMainId(mainid);
    }

    /**
     * 添加
     * @param map
     * @return
     */
    @Override
    public int addMain(Map map) {
        return carmaintainDao.addMain(map);
    }

    @Override
    public int updateMain(Map map) {
        return carmaintainDao.updateMain(map);
    }

    /**
      * @Description: 车辆报修
      * @Author zys
      * @Date 2020/7/1 0001 2:19
      * @Param [map]
     */
    @Transactional
    @Override
    public int addMaintain(Map map) {
        Integer user_id = (Integer) SessionUtil.getUserInfo().get("user_id");
        map.put("user_id",user_id);
        //修改维修状态
        carmaintainDao.updateMaintainState(map);
        //添加维修记录
        return carmaintainDao.addMaintain(map);
    }

    /**
      * @Description: 维修
      * @Author zys
      * @Date 2020/7/1 0001 2:57
      * @Param [map]
     */
    @Transactional
    @Override
    public int toMaintain(Map map) {
        //修改维修后的车辆状态
        carmaintainDao.updateState(map);
        return carmaintainDao.toMaintain(map);
    }
}
