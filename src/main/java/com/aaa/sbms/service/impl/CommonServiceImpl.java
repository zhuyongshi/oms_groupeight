package com.aaa.sbms.service.impl;

import com.aaa.sbms.service.CommonService;
import com.aaa.sbms.util.FtpProperties;
import com.aaa.sbms.util.FtpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.OutputStream;
import java.util.Map;

@Service
public class CommonServiceImpl implements CommonService {

    @Autowired
    private FtpProperties ftpProperties;

    @Override
    public Map uploadFile(MultipartFile file, String savePath) {
        return FtpUtil.localUpload(ftpProperties,file,savePath);
    }

    @Override
    public Boolean downloadFile(OutputStream outputStream,String fileName){
        return FtpUtil.downloadFile(ftpProperties,fileName,outputStream);
    }
}
