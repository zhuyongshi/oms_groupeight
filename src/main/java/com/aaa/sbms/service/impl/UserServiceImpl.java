package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.DeptDao;
import com.aaa.sbms.dao.UserDao;
import com.aaa.sbms.entity.Emp;
import com.aaa.sbms.service.UserService;
import com.aaa.sbms.util.PageUtil;
import com.aaa.sbms.util.SessionUtil;
import com.aaa.sbms.util.Sha512Util;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * fileName:UserServiceImpl
 * description:
 * author:zz
 * createTime:2019/11/26 16:38
 * version:1.0.0
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired(required = false)
    private UserDao userDao;

    @Autowired(required = false)
    private DeptDao deptDao;

    @Autowired
    private HttpSession session;

    /**
      * 功能描述
      * @Author zys
      * @Description //TODO
      * @Date 2020/6/11 0011 14:42
      * @Param []
      * @return java.util.List<java.util.Map>
     */
    @Override
    public Map getEmpList(Map map) {
        PageUtil.list(map);
        PageInfo<Map> pageInfo=new PageInfo<Map>(userDao.getEmpList(map));
        List<Map> dept = deptDao.listDept();
        Map emap=new HashMap();
        emap.put("depts",dept);
        emap.put("total",pageInfo.getTotal());
        emap.put("page",pageInfo.getList());
        return emap;
    }
    /**
      * @Description: 根据用户名或用户session
      * @Author zys
      * @Date 2020/6/13 0013 12:04
      * @Param [tel]
      * @return java.util.Map
     */
    @Override
    public Map getUserByUserName(String tel) {
        return userDao.getUserByUserName(tel);
    }

    /**
      * @Description: 获取用户信息
      * @Author zys
      * @Date 2020/6/17 0017 19:50
      * @Param [map]
      * @return java.util.Map
     */
    @Override
    public Map getList(Map map) {
        //获取所有用户
        List<Map> list = userDao.list(map);
        //循环遍历每一个用户
        for (Map map1 : list) {
            //获取用户ID
            Integer user_id = (Integer) map1.get("user_id");
            if (user_id != null) {
                //获取用户的角色
                List<Map> roles = userDao.getRoleByUserId(user_id);
                //把角色添加到当前用户
                map1.put("roles", roles);
            }
        }
        //获取分页数据
        PageUtil.list(map);
        PageInfo<Map> pageInfo = new PageInfo<Map>(list);
        Map roleMap  = new HashMap();
        roleMap.put("total",pageInfo.getTotal());
        roleMap.put("page",pageInfo.getList());
        return roleMap;
    }

    @Override
    public int add(Map map) {
        String salt="efdd1d36-2430-4";
        //管理人员添加新员工默认密码为 tiger
        String password="15da080f1a2c882e2b47a5f2183e01289fbcb9a3038bb149535e037b6efb43e514869a99060522ac148e97aced92fc6d37c99b914fffa29b76dcb9dd9a91d3ad";
        //随机盐值，放入数据库
        map.put("salt", salt);
        map.put("password",password);
        return userDao.add(map);
    }
   /**
     * @Author zys
     * @Description 更新员工信息
     * @Date 2020/6/12 0012 15:04
     * @Param [map]
     * @return int
    */
    @Override
    public int update(Map map) {
        return userDao.update(map);
    }

    /**
      * @Description: 删除用户级用户关联的角色
      * @Author zys
      * @Date 2020/6/13 0013 15:20
      * @Param [userId]
      * @return int
     */
    @Override
    public int delete(int userId) {
        //删除该用户级联的所有角色ID
        userDao.delteRoleIdsByUserId(userId);
        //删除用户(假删除)
        return userDao.delete(userId);
    }

    @Override
    public List<Integer> listRoleIdsByUserId(int userId) {
        return userDao.listRoleIdsByUserId(userId);
    }

    @Override
    public int delteRoleIdsByUserId(int userId) {
        return userDao.delteRoleIdsByUserId(userId);
    }

    @Override
    public int addUserAndRole(int userId, String roleIds) {
        try {
            //调用上面方法，删除该用户关联的所有角色
            this.delteRoleIdsByUserId(userId);
            StringBuffer stringBuffer =new StringBuffer();
            //分割字符串，循环拼接values后面  (1,1),(1,2),(1,10)...
            String[] ridArray = roleIds.split(",");
            for (String rid : ridArray) {
                stringBuffer.append("("+userId+","+rid+"),");
            }
            String values=stringBuffer.substring(0,stringBuffer.length()-1);
            userDao.addUserAndRole(values);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
      * @Description: 获取直系领导
      * @Author zys
      * @Date 2020/6/12 0012 19:45
      * @Param [deptId]
      * @return java.util.List<java.util.Map>
     */
    @Override
    public List<Map> getLeader(int deptId) {
        return userDao.getLear(deptId);
    }

    /**
      * @Description: 修改用户头像
      * @Author zys
      * @Date 2020/6/16 0016 21:27
      * @Param [imageUrl]
      * @return int
     */
    @Override
    public int updateAvatar(String imageUrl) {
        Integer user_id =(Integer) SessionUtil.getUserInfo().get("user_id");
        return userDao.updateAvatar(imageUrl,user_id);
    }
    /**
      * @Description: 获取个人信息
      * @Author zys
      * @Date 2020/6/17 0017 12:07
      * @Param []
      * @return java.util.List<java.util.Map>
     */
    @Override
    public Map getPerson() {
        Integer user_id = (Integer) SessionUtil.getUserInfo().get("user_id");
        return  userDao.getPerson(user_id);
    }

    /**
      * @Description: 更新个人信息
      * @Author zys
      * @Date 2020/6/17 0017 15:22
      * @Param [map]
      * @return int
     */
    @Override
    public int updatePerson(Map map) {
        Integer user_id = (Integer) SessionUtil.getUserInfo().get("user_id");
        map.put("user_id",user_id);
        return userDao.updatePerson(map);
    }

    /**
      * @Description:更新密码
      * @Author zys
      * @Date 2020/6/17 0017 16:07
      * @Param [map]
      * @return int
     */
    @Override
    public int updatePwd(String password) {
        Integer user_id =(Integer) SessionUtil.getUserInfo().get("user_id");
        //获取盐值和加密后的密码
        Map map = Sha512Util.pwd(password);
        map.put("user_id",user_id);
        System.out.println(map);
        return userDao.updatePwd(map);
    }

    /**
      * @Description: 删除用户角色
      * @Author zys
      * @Date 2020/6/17 0017 19:09
      * @Param [user_id]
      * @return int
     */
    @Override
    public int delRole(Integer user_id) {
        return userDao.delteRoleIdsByUserId(user_id);
    }

    /**
      * @Description: 获取员工信息
      * @Author zys
      * @Date 2020/6/26 0026 11:39
      * @Param []
     */
    @Override
    public List<Emp> getAllEmp() {
        List<Emp> list=new ArrayList<>();
        for (Emp e : userDao.getAllEmp()){
            Emp emp =new Emp();
            //工号
            emp.setWorkId(e.getWorkId());
            //姓名
            emp.setUsername(e.getUsername());
            //电话
            emp.setTel(e.getTel());
            //判断性别
            if (e.getSex()=="0" ||e.getSex().equals("0")){
                emp.setSex("男");
            }else  if(e.getSex()=="1" ||e.getSex().equals("1")){
                emp.setSex("女");
            }else {
                emp.setSex("未知");
            }
            //判断状态
            if (e.getStatus()=="0" ||e.getStatus().equals("0")){
                emp.setStatus("实习");
            }else  if(e.getStatus()=="1" ||e.getStatus().equals("1")){
                emp.setStatus("离职");
            }else {
                emp.setStatus("在职");
            }
            //身份证
            emp.setIdCard(e.getIdCard());
            //部门
            emp.setGroupId(e.getGroupId());
            //入职日期
            emp.setLockDay(e.getLockDay());
            list.add(emp);
        }
        return list;
    }
}
