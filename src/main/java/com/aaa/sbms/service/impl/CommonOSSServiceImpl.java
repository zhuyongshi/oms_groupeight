package com.aaa.sbms.service.impl;

import com.aaa.sbms.property.OssProperties;
import com.aaa.sbms.service.CommonOSSService;
import com.aaa.sbms.util.OSSBootUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * fileName:CommonOSSServiceImpl
 * description:
 * author:zz
 * createTime:2020/3/20 15:04
 * version:1.0.0
 */
@Service
public class CommonOSSServiceImpl implements CommonOSSService {
    @Autowired
    private OssProperties ossProperties;
    @Override
    public Map uploadOSS(MultipartFile file) throws Exception {
        // 高依赖版本 oss 上传工具
        String ossFileUrlBoot = null;
        ossFileUrlBoot = OSSBootUtil.upload(ossProperties, file, "upload/files");
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("ossFileUrlBoot", ossFileUrlBoot);
        resultMap.put("oldFileName", file.getOriginalFilename());
        return resultMap;
    }
}
