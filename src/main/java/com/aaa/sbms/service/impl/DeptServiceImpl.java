package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.DeptDao;
import com.aaa.sbms.service.DeptService;
import com.aaa.sbms.util.PageUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * className:DeptServiceImpl
 * description:
 * author:zys
 * createTime:2020/6/12 0012 15:48
 */
@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptDao deptDao;

    /**
      * @Description: 获取部门信息
      * @Author zys
      * @Date 2020/6/12 0012 15:50
      * @Param []
      * @return java.util.List<java.util.Map>
     */
    @Override
    public List<Map> listDept() {
        return deptDao.listDept();
    }
    /**
      * @Description:
      * @Author zys
      * @Date 2020/6/15 0015 22:28
      * @Param [map]
      * @return java.util.Map
     */
    @Override
    public Map getDeptList(Map map) {
        PageUtil.list(map);
        PageInfo<Map> pageInfo=new PageInfo<Map>(deptDao.getDeptList(map));
        List<Map> leaders = deptDao.getLeaders();
        Map dmap=new HashMap();
        dmap.put("total",pageInfo.getTotal());
        dmap.put("page",pageInfo.getList());
        dmap.put("leaders",leaders);
        return dmap;
    }
    /**
      * @Description:更新部门数据
      * @Author zys
      * @Date 2020/6/16 0016 13:12
      * @Param [map]
      * @return java.util.List<java.util.Map>
     */
    @Override
    public int update(Map map) {
        return deptDao.update(map);
    }

    /**
      * @Description:添加部门信息
      * @Author zys
      * @Date 2020/6/16 0016 14:11
      * @Param [map]
      * @return int
     */
    @Override
    public int add(Map map) {
        return deptDao.add(map);
    }

    /**
      * @Description: 删除部门
      * @Author zys
      * @Date 2020/6/16 0016 14:34
      * @Param [deptId]
      * @return int
     */
    @Override
    public int delete(Integer deptId) {
        return deptDao.delete(deptId);
    }
}
