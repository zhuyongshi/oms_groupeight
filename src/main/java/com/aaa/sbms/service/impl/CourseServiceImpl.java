package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.CoursesDao;
import com.aaa.sbms.service.CourseService;
import com.aaa.sbms.util.PageUtil;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程管理ServiceImpl
 * Fong
 */
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CoursesDao coursesDao;

    /**
     * 查询所有课程
     * @param map
     * @return
     */
    @Override
    public Map getCourseAll(Map map) {
        PageUtil.list(map);
        Page<Map> page = (Page<Map>) coursesDao.getCourseAll(map);
        Map map1 = new HashMap();
        map1.put("total",page.getTotal());
        map1.put("page",page.getResult());
        return map1;
    }

    /**
     * 根据ID删除课程
     * @param courseId
     * @return
     */
    @Override
    public int deleteCourseById(int courseId) {
        return coursesDao.deleteCourseById(courseId);
    }

    /**
     * 添加课程信息
     * @param map
     * @return
     */
    @Override
    public int addCourse(Map map) {
        return coursesDao.addCourse(map);
    }

    /**
     * 编辑课程信息
     * @param map
     * @return
     */
    @Override
    public int updataCourse(Map map){
        return coursesDao.updataCourse(map);
    }

    /**
     * 批量删除
     * @param list
     * @return
     */
    @Override
    public int deleteCourses(List<Integer> list) {
        int num=0;
        for (int courseId : list){
            coursesDao.deleteCourseById(courseId);
            num++;
        }
        return num;
    }
}
