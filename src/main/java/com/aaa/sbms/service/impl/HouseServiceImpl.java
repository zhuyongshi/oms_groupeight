package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.HouseDao;
import com.aaa.sbms.service.HouseService;
import com.aaa.sbms.util.AddressUtil;
import com.aaa.sbms.util.PageUtil;
import com.aaa.sbms.util.SessionUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * className:HouseServiceImpl
 * description:
 * author:zys
 * createTime:2020/6/19 0019 12:25
 */
@Service
public class HouseServiceImpl implements HouseService {

    @Autowired
    private HouseDao houseDao;

    @Override
    public Map listHouse(Map map) {
        PageUtil.list(map);
        List<Map> mapList = houseDao.listHouse(map);
        //遍历拆分地址
        for (Map map1 : mapList) {
            String address = (String) map1.get("address");
            //分割地址，获取省市区一级详细地址
            String[] split = address.split(",");
            map1.put("provinceValue", split[0]);
            map1.put("cityValue", split[1]);
            map1.put("areaValue", split[2]);
            map1.put("ADDRESS", split[3]);
            String replace = address.replace(",", "");
            map1.put("addr", replace);
            //获取每栋宿舍的ID
            Integer houseId = (Integer) map1.get("houseId");
            //查询每栋宿舍的楼层数
            List<Map> listFloor = houseDao.listFloor(houseId);
            map1.put("listFloor", listFloor);
            for (Map map2 : listFloor) {
                Integer floor = (Integer) map2.get("floor");
                List<Map> listRooms = houseDao.listRooms(floor, houseId);
                map2.put("listRooms",listRooms);
            }
        }
        //分页
        PageInfo pageInfo=new PageInfo(mapList);
        Map houseMap=new HashMap();
        houseMap.put("page",pageInfo.getList());
        houseMap.put("total",pageInfo.getTotal());
        return houseMap;
    }

    /**
      * @Description: 添加宿舍
      * @Author zys
      * @Date 2020/6/19 0019 16:43
      * @Param [map]
     */
    @Override
    public int addHouse(Map map) {
        AddressUtil.Addr(map);
        return houseDao.addHouse(map);
    }

    /**
      * @Description: 更新宿舍
      * @Author zys
      * @Date 2020/6/19 0019 20:36
      * @Param [map]
     */
    @Override
    public int updateHouse(Map map) {
        AddressUtil.Addr(map);
        return houseDao.updateHouse(map);
    }

    /**
      * @Description: 获取房间号
      * @Author zys
      * @Date 2020/6/20 0020 10:52
      * @Param [floor, houseId]
     */
    @Override
    public List<Map> getRooms(Integer floor, Integer houseId) {
        return houseDao.listRooms(floor,houseId);
    }

    /**
      * @Description: 添加楼层 房间 床位
      * @Author zys
      * @Date 2020/6/20 0020 15:19
      * @Param [map]
     */
    @Override
    public int addFloor(Map map) {
        //判断当前添加的楼层是否存在
        int count = houseDao.isListFloor(map);
        if (count==0) {
            // 添加楼层
            return houseDao.addFloor(map);
        }
        else {
            //获取要添加的床位数
            String bedId = map.get("bed") + "";
            Integer bedNum = Integer.valueOf(bedId);
            //删除已经存在的床位
            houseDao.deleteBeds(map);
            //循化添加床位
            for (int i = 1; i <= bedNum; i++) {
                map.put("bedId", i);
                houseDao.addBeds(map);
            }
            return 0;
        }

    }

    /**
      * @Description: 获取床位号
      * @Author zys
      * @Date 2020/6/21 0021 12:58
      * @Param [houseId, floorId, roomId]
     */
    @Override
    public List<Map> listBeds(Integer houseId, Integer floorId, Integer roomId) {
        return houseDao.listBeds(houseId,floorId,roomId);
    }

    /**
      * @Description: 根据宿舍id删除宿舍
      * @Author zys
      * @Date 2020/6/21 0021 16:12
      * @Param [houseId]
     */
    @Override
    public int deleteByHouseId(Integer houseId) {
        //判断该宿舍是否有房间
        List<Map> mapList = houseDao.listHouseItemsByHouseId(houseId);
        //如果没有
        if (mapList.size()==0){
            //删除该宿舍
            return  houseDao.deleteHouseByHouseId(houseId);
        }else {
            return 0;
        }
    }

    /**
      * @Description: 修改状态
      * @Author zys
      * @Date 2020/6/21 0021 17:30
      * @Param [id]
     */
    @Override
    public int changeStatus(Integer id,Integer sta) {
        Integer status;
        if (sta==0){
            status=1;
        }else {
            status=0;
        }
        return houseDao.changeStatus(id,status);
    }

    /**
      * @Description: 添加维修
      * @Author zys
      * @Date 2020/6/23 0023 20:49
      * @Param [map]
     */
    @Transactional
    @Override
    public int addRepair(Map map) {
        Integer workId;
        //获取hId 外键
        int hId = houseDao.getHIdByHFR(map);
        System.out.println(hId);
        map.put("hId",hId);
        //获取当前报修人工号
        String  work =SessionUtil.getUserInfo().get("workId")+"";
        workId= Integer.valueOf(work);
        map.put("workId",workId);
        //添加维修表信息
        houseDao.addHouseRepair(map);
        //修改房子使用状态
        return houseDao.addRepair(map);
    }
}
