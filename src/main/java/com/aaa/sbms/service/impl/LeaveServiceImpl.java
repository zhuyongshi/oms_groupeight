package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.LeaveDao;
import com.aaa.sbms.service.LeaveService;
import com.aaa.sbms.util.PageUtil;
import com.aaa.sbms.util.SessionUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * className:LeaveServiceImpl
 * description:
 * author:zys
 * createTime:2020/6/29 0029 20:21
 */
@Service
public class LeaveServiceImpl  implements LeaveService {

    @Autowired
    private LeaveDao leaveDao;

    /**
      * @Description: 获取人事审核信息
      * @Author zys
      * @Date 2020/6/29 0029 20:27
      * @Param [map]
     */
    @Override
    public Map listFirstRecord(Map map) {
        PageUtil.list(map);
        PageInfo pageInfo=new PageInfo(leaveDao.listFirstRecord(map));
        Map firstMap=new HashMap();
        firstMap.put("page",pageInfo.getList());
        firstMap.put("total",pageInfo.getTotal());
        return firstMap;
    }

    /**
      * @Description: 批准
      * @Author zys
      * @Date 2020/6/29 0029 20:58
      * @Param [map]
     */
    @Override
    public int toPass(Map map) {
        return leaveDao.toPass(map);
    }

    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/29 0029 21:05
      * @Param [map]
     */
    @Override
    public int toUnPass(Map map) {
        String workId =(String) SessionUtil.getUserInfo().get("workId");
        map.put("checkId",workId);
        return leaveDao.toUnpass(map);
    }

    /**
      * @Description: 获取审核记录信息
      * @Author zys
      * @Date 2020/6/29 0029 21:20
      * @Param [map]
     */
    @Override
    public Map listHistoryList(Map map) {
        PageUtil.list(map);
        PageInfo pageInfo=new PageInfo(leaveDao.listHistoryList(map));
        Map historyMap=new HashMap();
        historyMap.put("page",pageInfo.getList());
        historyMap.put("total",pageInfo.getTotal());
        return historyMap;
    }

    /**
      * @Description: 获取领导审核信息
      * @Author zys
      * @Date 2020/6/29 0029 21:39
      * @Param [map]
     */
    @Override
    public Map listTwoRecord(Map map) {
        PageUtil.list(map);
        PageInfo pageInfo=new PageInfo(leaveDao.listTwoRecord(map));
        Map twoMap=new HashMap();
        twoMap.put("page",pageInfo.getList());
        twoMap.put("total",pageInfo.getTotal());
        return twoMap;
    }

    /**
      * @Description: 二次批准
      * @Author zys
      * @Date 2020/6/29 0029 21:43
      * @Param [map]
     */
    @Override
    public int toTowPass(Map map) {
        String workId =(String) SessionUtil.getUserInfo().get("workId");
        map.put("checkId",workId);
        return leaveDao.toTowPass(map);
    }

    /**
      * @Description: 二次驳回
      * @Author zys
      * @Date 2020/6/29 0029 21:47
      * @Param [map]
     */
    @Override
    public int toTowUnPass(Map map) {
        return leaveDao.toTowUnPass(map);
    }

    /**
      * @Description: 请假
      * @Author zys
      * @Date 2020/6/30 0030 16:43
      * @Param [map]
     */
    @Override
    public int toAddLeave(Map map) {
        //获取工号
        String  workId =(String) SessionUtil.getUserInfo().get("workId");
        //获取电话
        String  tel =(String) SessionUtil.getUserInfo().get("tel");
        //获取请假开始结束时间，计算请假天数
        String beginTime =(String) map.get("beginTime");
        String endTime =(String) map.get("endTime");
        String bTime = beginTime.replace("-", "");
        String eTime = endTime.replace("-", "");
        Integer start = Integer.valueOf(bTime);
        Integer end = Integer.valueOf(eTime);
        //计算请假天数
        Integer num=end-start;
        map.put("tel",tel);
        map.put("workId",workId);
        map.put("num",num);
        return leaveDao.toAddLeave(map);
    }

    /**
      * @Description: 查询结果
      * @Author zys
      * @Date 2020/6/30 0030 17:53
      * @Param []
     */
    @Override
    public String checkResult() {
        String workId = (String) SessionUtil.getUserInfo().get("workId");
        Map map = leaveDao.checkResult(workId);
        Integer firstcheck =(Integer) map.get("firstcheck");
        Integer twocheck =(Integer) map.get("twocheck");
        if (firstcheck==1 && twocheck==1){
            return "已通过";
        }else {
            return "待审核，请耐心等待";
        }
    }
}
