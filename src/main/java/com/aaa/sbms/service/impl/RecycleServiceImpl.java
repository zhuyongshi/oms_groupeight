package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.RecycleDao;
import com.aaa.sbms.service.RecycleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 回收站
 * @author 10744
 */
@Service
public class RecycleServiceImpl implements RecycleService {

    @Autowired
    private RecycleDao recycleDao;

    /**
     * 回收站文件列表
     * @param userId
     * @return
     */
    @Override
    public List<Map> getFileList(int userId) {
        return recycleDao.getFileList(userId);
    }

    /**
     * 彻底删除文件
     * @param fileId
     * @return
     */
    @Override
    public int deleteFile(Integer fileId) {
        return recycleDao.deleteFile(fileId);
    }

    /**
     * 还原文件
     * @param fileId
     * @return
     */
    @Override
    public int rollback(Integer fileId) {
        return recycleDao.rollback(fileId);
    }
}
