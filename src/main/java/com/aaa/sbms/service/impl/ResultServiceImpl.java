package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.CoursePlanDao;
import com.aaa.sbms.dao.ResultDao;
import com.aaa.sbms.service.ResultService;
import com.aaa.sbms.util.PageUtil;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 考核结果
 * @author 10744
 */
@Service
public class ResultServiceImpl implements ResultService {

    @Autowired
    private ResultDao resultDao;

    @Autowired
    private CoursePlanDao coursePlanDao;

    @Override
    public Map getAll(Map map) {
        PageUtil.list(map);
        Page<Map> page = (Page<Map>) resultDao.getAll(map);
        Map rmap  = new HashMap();
        rmap.put("total",page.getTotal());
        rmap.put("page",page.getResult());
        return rmap;
    }

    @Override
    public List<Map> getForm() {
        Map map = new HashMap();
        map.put("cpName",null);
        return coursePlanDao.getCoursePlanAll(map);
    }

    @Override
    public int update(Map map) {
        return resultDao.update(map);
    }
}
