package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.CarDao;
import com.aaa.sbms.service.CarService;
import com.aaa.sbms.util.PageUtil;
import com.aaa.sbms.util.SessionUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class CarServiceImpl implements CarService {

    @Autowired(required = false)
    private CarDao carDao;

    /**
     * 查询所有的车辆信息
     * @param map
     * @return
     */
    @Override
    public Map selectAllCar(Map map) {
        PageUtil.list(map);
        PageInfo<Map> pageInfo = new PageInfo<Map>(carDao.selectAllCat(map));
        List<Map> user = carDao.getUser();
        Map rmap = new HashMap();
        rmap.put("users",user);
        rmap.put("total",pageInfo.getTotal());
        rmap.put("page",pageInfo.getList());
        return rmap;
    }

    /**
     * 删除
     * @param carid
     * @return
     */
    @Override
    public int deleteCarId(Integer carid) {
        return carDao.deleteCarId(carid);
    }

    /**
     * 添加
     * @param map
     * @return
     */
    @Override
    public int addCar(Map map) {
        return carDao.addCar(map);
    }

    /**
     * 更新
     * @param map
     * @return
     */
    @Override
    public int updataCar(Map map) {
        return carDao.updateCar(map);
    }

    /**
      * @Description: 获取可用车辆
      * @Author zys
      * @Date 2020/6/30 0030 21:44
      * @Param []
     */
    @Override
    public List<Map> getUserCar() {
        return carDao.getUserCar();
    }

    /**
      * @Description: 申请用车
      * @Author zys
      * @Date 2020/6/30 0030 21:58
      * @Param [map]
     */
    @Override
    public int toUseCar(Map map) {
        Integer user_id =(Integer) SessionUtil.getUserInfo().get("user_id");
        map.put("user_id",user_id);
        return carDao.toUseCar(map);
    }

    /**
     * 通过id查询用户的名称
     * @param map
     * @return
     */

}
