package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.CoursePlanDao;
import com.aaa.sbms.dao.CoursesDao;
import com.aaa.sbms.dao.TeacherDao;
import com.aaa.sbms.entity.Teacher;
import com.aaa.sbms.service.TeacherService;
import com.aaa.sbms.util.PageUtil;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 教师管理
 * Fong
 */
@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherDao teacherDao;

    @Autowired
    private CoursePlanDao coursePlanDao;

    @Autowired
    private CoursesDao coursesDao;

    /**
     * 查询所有教师信息
     * @param map
     * @return
     */
    @Override
    public Map selectAllTeacher(Map map) {
        PageUtil.list(map);
        Page<Map> page = (Page<Map>) teacherDao.selectAllTeacher(map);
        Map rmap  = new HashMap();
        rmap.put("total",page.getTotal());
        rmap.put("page",page.getResult());
        return rmap;
    }

    /**
     * 删除教师信息
     * @param teacherId
     * @return
     */
    @Override
    public int deleteTeacherById(int teacherId) {
        return teacherDao.deleteTeacherById(teacherId);
    }

    /**
     * 添加教师信息
     * @param map
     * @return
     */
    @Override
    public int addTeacher(Map map) {
        return teacherDao.addTeacher(map);
    }

    /**
     * 编辑教师信息
     * @param map
     * @return
     */
    @Override
    public int updateTeacher(Map map) {
        Integer status = Integer.valueOf(map.get("t_status").toString()) ;
        String name = (String) map.get("t_name");
        Map courseName = coursePlanDao.getCourse(name);
        String cName = (String)courseName.get("cp_cname");
        Map course = coursesDao.getNumber(cName);
        Integer cNumber = Integer.valueOf(course.get("c_number").toString());
        if (status==1 || status==3){
            if (cNumber>0){
                coursesDao.changeCourseNumber(cName,cNumber-1);
            }
            coursePlanDao.changeCoursePlanStatus(name,2);
        }
        return teacherDao.updateTeacher(map);
    }

    /**
     * 批量删除
     * @param list
     * @return
     */
    @Override
    public int deleteDatas(List<Integer> list){
        int num=0;
        for (int id : list){
            teacherDao.deleteTeacherById(id);
            num++;
        }
        return num;
    }

    @Override
    public Map getName() {
        List<Map> list = teacherDao.getTeacherName();
        Map map=new HashMap();
        map.put("result",list);
        return map;
    }

    @Override
    public List<Teacher> getAllTeacher() {
        List<Teacher> list=new ArrayList<>();
        for (Teacher t : teacherDao.getAllTeacher()){
            Teacher teacher =new Teacher();
            teacher.setName(t.getName());
            teacher.setSex(t.getSex());
            teacher.setInfo(t.getInfo());
            if (t.getStatus()=="1" || t.getStatus().equals("1")){
                teacher.setStatus("空闲");
            }else if (t.getStatus()=="2" || t.getStatus().equals("2")){
                teacher.setStatus("在教");
            }else if (t.getStatus()=="3" || t.getStatus().equals("3")){
                teacher.setStatus("出差");
            }else if (t.getStatus()=="5" || t.getStatus().equals("5")){
                teacher.setStatus("假期");
            }else {
                teacher.setStatus("辞职");
            }
            list.add(teacher);
        }
        return list;
    }


}
