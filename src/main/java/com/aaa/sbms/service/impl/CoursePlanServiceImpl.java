package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.CoursePlanDao;
import com.aaa.sbms.dao.CoursesDao;
import com.aaa.sbms.dao.TeacherDao;
import com.aaa.sbms.service.CoursePlanService;
import com.aaa.sbms.util.PageUtil;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程计划
 * Fong
 */
@Service
public class CoursePlanServiceImpl implements CoursePlanService {

    @Autowired
    private CoursePlanDao coursePlanDao;

    @Autowired
    private TeacherDao teacherDao;

    @Autowired
    private CoursesDao coursesDao;

    /**
     * 查询所有
     *
     * @param map
     * @return
     */
    @Override
    public Map getCoursePlanAll(Map map) {
        PageUtil.list(map);
        Page page = (Page<Map>) coursePlanDao.getCoursePlanAll(map);
        Map map1 = new HashMap();
        map1.put("total", page.getTotal());
        map1.put("page", page.getResult());
        return map1;
    }

    /**
     * 根据ID删除课程计划
     *
     * @param coursePlanId
     * @return
     */
    @Override
    public int deleteCoursePlanById(int coursePlanId) {
        return coursePlanDao.deleteCoursePlanById(coursePlanId);

    }

    /**
     * 添加课程
     *
     * @param map
     * @return
     */
    @Override
    public int addCoursePlan(Map map) {
        return coursePlanDao.addCoursePlan(map);
    }

    @Override
    public Map getFormDate() {
        List<Map> teacherList = teacherDao.getTeacherName();
        List<Map> courseList = coursesDao.getCourseName();
        Map map = new HashMap();
        map.put("teacher", teacherList);
        map.put("course", courseList);
        return map;
    }

    /**
     * 编辑课程计划
     *
     * @param map
     * @return
     */
    @Override
    public int updateCoursePlan(Map map) {
        Integer cpStatus = Integer.valueOf(map.get("cp_status").toString());
        String tName = (String) map.get("t_name");
        String cName = (String) map.get("cp_cname");
        Map course = coursesDao.getNumber(cName);
        Integer cNumber = Integer.valueOf(course.get("c_number").toString());
        System.out.println(cpStatus + "----------------" + tName);
        if (cpStatus == 1) {
            coursesDao.changeCourseNumber(cName,cNumber+1);
            //课程计划开课时，教师状态改为在教
            teacherDao.changeTeacherStatus(tName, 2);
            return coursePlanDao.updateCoursePlan(map);
        } else if (cpStatus == 2 || cpStatus == 3) {
            if (cNumber>0){
                coursesDao.changeCourseNumber(cName,cNumber-1);
            }
            //课程计划停课，教师状态改为空闲
            teacherDao.changeTeacherStatus(tName, 1);
            return coursePlanDao.updateCoursePlan(map);
        }
        return 500;
    }
}
