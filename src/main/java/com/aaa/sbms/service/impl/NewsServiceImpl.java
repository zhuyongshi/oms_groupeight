package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.NewsDao;
import com.aaa.sbms.service.NewsService;
import com.aaa.sbms.util.FtpProperties;
import com.aaa.sbms.util.FtpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * fileName:NewsServiceImpl
 * description:
 * author:zz
 * createTime:2019/11/26 14:14
 * version:1.0.0
 */
@Service
//@Transactional
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDao newsDao;
    @Autowired
    private FtpProperties ftpProperties;

    @Override
    public List<Map> getList() {
        return newsDao.getList();
    }

    @Override
    public Map getPage(Map map) {
        //开始值： begin =(pageNo-1)*pageSize
        //结束值： end  =pageNo*pageSize+1
        //获取前面输入值，计算后放入参数中
        map.put("begin",(Integer.valueOf(map.get("pageNo")+"")-1)*Integer.valueOf(map.get("pageSize")+""));
        map.put("end",Integer.valueOf(map.get("pageNo")+"")*Integer.valueOf(map.get("pageSize")+"")+1);
        //调用 dao获取数据
        List<Map> page = newsDao.getPage(map);
        int total = newsDao.getPageCount(map);
        //合并结果
        Map resultMap = new HashMap();
        resultMap.put("page",page);
        resultMap.put("total",total);
        return resultMap;
    }

    @Override
    public int add(Map map) {
        return newsDao.add(map);
    }

    @Override
    public int update(Map map) {
        return newsDao.update(map);
    }

    @Override
    public int delete(int id) {
        return newsDao.delete(id);
    }
}
