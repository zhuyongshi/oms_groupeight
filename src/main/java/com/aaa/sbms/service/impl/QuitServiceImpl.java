package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.QuitDao;
import com.aaa.sbms.service.QuitService;
import com.aaa.sbms.util.PageUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * className:QuitServiceImpl
 * description:
 * author:zys
 * createTime:2020/6/24 0024 15:14
 */
@Service
public class QuitServiceImpl implements QuitService {

    @Autowired
    private QuitDao quitDao;

    /**
      * @Description: 获取离职记录表
      * @Author zys
      * @Date 2020/6/24 0024 15:17
      * @Param [map]
     */
    @Override
    public Map listQuit(Map map) {
        PageUtil.list(map);
        PageInfo pageInfo=new PageInfo(quitDao.listQuit(map));
        Map quitMap=new HashMap();
        quitMap.put("page",pageInfo.getList());
        quitMap.put("total",pageInfo.getTotal());
        return quitMap;
    }

    /**
      * @Description: 获取待审核离职信息
      * @Author zys
      * @Date 2020/6/24 0024 17:01
      * @Param [map]
     */
    @Override
    public Map listsRecord(Map map) {
        PageUtil.list(map);
        PageInfo pageInfo=new PageInfo(quitDao.listsRecord(map));
        Map recordMap=new HashMap();
        recordMap.put("page",pageInfo.getList());
        recordMap.put("total",pageInfo.getTotal());
        return recordMap;
    }

    /**
      * @Description: 通过审批
      * @Author zys
      * @Date 2020/6/24 0024 17:26
      * @Param [map]
     */
    @Override
    public int updateCheckState(Map map) {
        return quitDao.updateCheckState(map);
    }

    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/24 0024 17:32
      * @Param [map]
     */
    @Override
    public int changeCheckState(Map map) {
        return quitDao.changeCheckState(map);
    }

    /**
      * @Description: 提交离职申请
      * @Author zys
      * @Date 2020/6/29 0029 13:31
      * @Param [map]
     */
    @Override
    public int toLeave(Map map) {
        String male;
        String sex =(String) map.get("sex");
        if (sex.equals("0")){
            male="男";
        }else{
            male="女";
        }
        map.put("male",male);
        return quitDao.toLeave(map);
    }
}
