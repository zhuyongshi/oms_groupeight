package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.RepairDao;
import com.aaa.sbms.service.RepairService;
import com.aaa.sbms.util.PageUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * className:RepairServiceImpl
 * description:
 * author:zys
 * createTime:2020/6/23 0023 16:42
 */
@Service
public class RepairServiceImpl implements RepairService {

    @Autowired(required = false)
    private RepairDao repairDao;

    @Override
    public Map listRepair(Map map) {
        String checkInAddr;
        PageUtil.list(map);
        List<Map> mapList = repairDao.listRepair(map);
        //拼接地址
        for (Map map1 : mapList) {
            String address =(String) map1.get("address");
            String replaceAddr = address.replace(",", "");
            Integer room =(Integer) map1.get("room");
            Integer floor =(Integer) map1.get("floor");
            checkInAddr=replaceAddr+floor+'楼'+room+'房';
            map1.put("checkInAddr",checkInAddr);
        }
        PageInfo pageInfo=new PageInfo(mapList);
        Map listMap=new HashMap();
        listMap.put("page",pageInfo.getList());
        listMap.put("total",pageInfo.getTotal());
        return listMap;
    }

    /**
      * @Description: 维系处理
      * @Author zys
      * @Date 2020/6/23 0023 17:21
      * @Param [map]
     */
    @Transactional
    @Override
    public int toHandle(Map map) {
        //修改房间修理状态
        repairDao.updateRoomsStatus(map);
        return repairDao.toHandle(map);
    }
}
