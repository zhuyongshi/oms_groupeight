package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.CheckOutDao;
import com.aaa.sbms.service.CheckOutService;
import com.aaa.sbms.util.PageUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * className:CheckOutServiceImpl
 * description:
 * author:zys
 * createTime:2020/6/23 0023 14:37
 */@Service
public class CheckOutServiceImpl implements CheckOutService {


    @Autowired
    private CheckOutDao checkOutDao;

    /**
      * @Description: 获取退宿列表
      * @Author zys
      * @Date 2020/6/23 0023 14:51
      * @Param [map]
     */
    @Override
    public Map listCheckOut(Map map) {
        String checkInAddr;
        PageUtil.list(map);
        List<Map> mapList = checkOutDao.listCheckOut(map);
        //拼接地址
        for (Map map1 : mapList) {
            String address =(String) map1.get("address");
            String replaceAddr = address.replace(",", "");
            Integer floor =(Integer) map1.get("floor");
            Integer room =(Integer) map1.get("room");
            Integer bedId =(Integer) map1.get("bedId");
            checkInAddr=replaceAddr+floor+'楼'+room+'房'+bedId+'床';
            map1.put("checkInAddr",checkInAddr);
        }
        PageInfo pageInfo=new PageInfo(mapList);
        Map listMap=new HashMap();
        listMap.put("page",pageInfo.getList());
        listMap.put("total",pageInfo.getTotal());
        return listMap;
    }

    /**
      * @Description: 办理退宿
      * @Author zys
      * @Date 2020/6/23 0023 15:16
      * @Param [id]
     */
    @Transactional
    @Override
    public int toCheckOut(Map map) {
        //退宿修改宿舍状态，退宿日期
        checkOutDao.toCheckOut(map);
        // 修改床位状态
        int num = checkOutDao.updateBedStatus(map);
        if (num>0){
            //修改房间使用状态
            checkOutDao.updateRoomStatus(map);
        }
        //修改用户入住状态
        int count = checkOutDao.changeUserCheckinStatus(map);
        return count;
    }
}
