package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.CloudDao;
import com.aaa.sbms.service.CloudService;
import com.aaa.sbms.service.CommonService;
import com.aaa.sbms.util.FtpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * 在线网盘
 * Fong
 * @author 10744
 */
@Service
public class CloudServiceImpl implements CloudService {

    @Autowired
    private CloudDao cloudDao;
    @Autowired
    private CommonService commonService;


    /**
     * 上传文件
     * @param userId
     * @param file
     * @return
     */
    @Override
    public int upload(int userId, MultipartFile file){
        Map map = commonService.uploadFile(file, "");
        String newFilePath = map.get("newFilePath").toString();
        String oldFileName=map.get("oldFileName").toString();
        return cloudDao.upload(oldFileName,userId,newFilePath);
    }

    /**
     * 文件列表
     * @param userId
     * @return
     */
    @Override
    public List<Map> getFileList(int userId) {
        return cloudDao.getFileList(userId);

    }

    /**
     * 获得文件名字
     * @param fileId
     * @return
     */
    @Override
    public Map getFileByid(int fileId) {
        return cloudDao.getFileByid(fileId);
    }

    /**
     * 更新文件名字
     * @param map
     * @return
     */
    @Override
    public int updateFileName(Map map) {
        return cloudDao.updateFileName(map);
    }

    /**
     * 删除文件（移到回收站）
     * @param fileId
     * @return
     */
    @Override
    public int deleteFile(Integer fileId) {
        return cloudDao.deleteFile(fileId);
    }

}
