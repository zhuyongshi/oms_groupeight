package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.AutidDao;
import com.aaa.sbms.service.AutideService;
import com.aaa.sbms.util.PageUtil;
import com.aaa.sbms.util.SessionUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AutidServiceImpl implements AutideService {
    @Autowired(required = false)
    private AutidDao autidDao;

    /**
     * 查询所有
     * @param map
     * @return
     */
    @Override
    public Map selectAllAutid(Map map) {
        PageUtil.list(map);
        PageInfo<Map> pageInfo = new PageInfo<Map>(autidDao.selectAllAutid(map));
        Map rmap = new HashMap();
        rmap.put("total",pageInfo.getTotal());
        rmap.put("page",pageInfo.getList());
        return rmap;
    }


    /**
     * 审核
     * @param map
     * @return
     */
    @Transactional
    @Override
    public int updateAutidId(Map map) {
        Integer user_id =(Integer) SessionUtil.getUserInfo().get("user_id");
        System.out.println(user_id);
        map.put("user_id)",user_id);
        //修改汽车使用状态
        autidDao.updateUseStatus(map);
        return autidDao.updateAutidId(map);
    }

    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/30 0030 23:08
      * @Param [map]
     */
    @Override
    public int unPass(Map map) {
        Integer user_id =(Integer) SessionUtil.getUserInfo().get("user_id");
        map.put("user_id)",user_id);
        return autidDao.unPass(map);
    }

    /**
      * @Description: 获取当前用户借用的车辆
      * @Author zys
      * @Date 2020/6/30 0030 23:37
      * @Param []
     */
    @Override
    public Map getUserCarByUserId() {
        Integer user_id =(Integer) SessionUtil.getUserInfo().get("user_id");
        return autidDao.getUserCarByUserId(user_id);
    }

    /**
      * @Description: 归还车辆
      * @Author zys
      * @Date 2020/7/1 0001 0:26
      * @Param [map]
     */
    @Transactional
    @Override
    public int updaterState(Map map) {
        autidDao.updateCarStatus(map);
        return autidDao.updaterState(map);
    }
}
