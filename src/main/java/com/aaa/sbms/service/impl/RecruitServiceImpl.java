package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.DeptDao;
import com.aaa.sbms.dao.RecruitDao;
import com.aaa.sbms.service.RecruitService;
import com.aaa.sbms.util.PageUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * className:RecruitServiceImpl
 * description:
 * author:zys
 * createTime:2020/6/25 0025 11:07
 */
@Service
public class RecruitServiceImpl implements RecruitService {

    @Autowired
    private RecruitDao recruitDao;

    @Autowired
    private DeptDao deptDao;

    /**
      * @Description: 获取招聘信息
      * @Author zys
      * @Date 2020/6/25 0025 11:11
      * @Param [map]
     */
    @Override
    public Map listInfo(Map map) {
        PageUtil.list(map);
        List<Map> listDept = deptDao.listDept();
        PageInfo pageInfo=new PageInfo(recruitDao.listInfo(map));
        Map infoMap=new HashMap();
        infoMap.put("page",pageInfo.getList());
        infoMap.put("total",pageInfo.getTotal());
        infoMap.put("depts",listDept);
        return infoMap;
    }

    /**
      * @Description: 更新招聘信息状态
      * @Author zys
      * @Date 2020/6/25 0025 12:58
      * @Param [id]
     */
    @Override
    public int updateStateById(Integer id) {
        return recruitDao.updateStateById(id);
    }

    /**
      * @Description: 更新信息
      * @Author zys
      * @Date 2020/6/25 0025 13:26
      * @Param [map]
     */
    @Override
    public int update(Map map) {
        return recruitDao.update(map);
    }

    /**
      * @Description: 添加信息
      * @Author zys
      * @Date 2020/6/25 0025 13:27
      * @Param [map]
     */
    @Override
    public int add(Map map) {
        return recruitDao.add(map);
    }

    /**
      * @Description: 入职审核记录
      * @Author zys
      * @Date 2020/6/25 0025 14:15
      * @Param [map]
     */
    @Override
    public Map recordList(Map map) {
        PageUtil.list(map);
        PageInfo pageInfo=new PageInfo(recruitDao.recordList(map));
        Map recordMap=new HashMap();
        recordMap.put("page",pageInfo.getList());
        recordMap.put("total",pageInfo.getTotal());
        return recordMap;
    }

    /**
      * @Description: 获取待审核应聘信息
      * @Author zys
      * @Date 2020/6/25 0025 15:44
      * @Param [map]
     */
    @Override
    public Map listCheck(Map map) {
        PageUtil.list(map);
        PageInfo pageInfo=new PageInfo(recruitDao.listCheck(map));
        Map checkMap=new HashMap();
        checkMap.put("page",pageInfo.getList());
        checkMap.put("total",pageInfo.getTotal());
        return checkMap;
    }

    /**
      * @Description: 审批
      * @Author zys
      * @Date 2020/6/25 0025 16:00
      * @Param [map]
     */
    @Override
    public int pass(Map map) {
        return recruitDao.pass(map);
    }

    /**
      * @Description: 驳回申请
      * @Author zys
      * @Date 2020/6/25 0025 16:07
      * @Param [map]
     */
    @Override
    public int unPass(Map map) {
        return recruitDao.unPass(map);
    }

    /**
      * @Description: 入职新员工
      * @Author zys
      * @Date 2020/6/25 0025 16:28
      * @Param [map]
     */
    @Override
    public Map newList(Map map) {
        PageUtil.list(map);
        PageInfo pageInfo=new PageInfo(recruitDao.newList(map));
        Map newMap=new HashMap();
        newMap.put("page",pageInfo.getList());
        newMap.put("total",pageInfo.getTotal());
        return newMap;
    }

    /**
      * @Description: 驳回新员工入职
      * @Author zys
      * @Date 2020/6/25 0025 17:28
      * @Param [map]
     */
    @Override
    public int leave(Integer id) {
        return recruitDao.leave(id);
    }

    /**
      * @Description: 入职新员工
      * @Author zys
      * @Date 2020/6/25 0025 18:00
      * @Param [map]
     */
    @Transactional
    @Override
    public int addEmp(Map map) {
        String salt="efdd1d36-2430-4";
        //管理人员添加新员工默认密码为 tiger
        String password="15da080f1a2c882e2b47a5f2183e01289fbcb9a3038bb149535e037b6efb43e514869a99060522ac148e97aced92fc6d37c99b914fffa29b76dcb9dd9a91d3ad";
        //随机盐值，放入数据库
        map.put("salt", salt);
        map.put("password",password);
        recruitDao.changeResult(map);
        return recruitDao.addEmp(map);
    }

    /**
      * @Description: 添加简历
      * @Author zys
      * @Date 2020/6/28 0028 21:22
      * @Param [map]
     */
    @Override
    public int addResume(Map map) {
        return recruitDao.addResume(map);
    }
}
