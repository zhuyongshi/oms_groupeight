package com.aaa.sbms.service.impl;

import com.aaa.sbms.dao.CaruseDao;
import com.aaa.sbms.service.CarUserService;
import com.aaa.sbms.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CarUserServiceImpl implements CarUserService {
    @Autowired(required = false)
    private CaruseDao caruseDao;

    /**
     * 查询所有
     * @param map
     * @return
     */
    @Override
    public Map selectAllUse(Map map) {
        PageUtil.list(map);
        PageInfo<Map> pageInfo = new PageInfo<Map>(caruseDao.selectAllUse(map));
        Map rmap=new HashMap();
        rmap.put("total",pageInfo.getTotal());
        rmap.put("page",pageInfo.getList());
        return rmap;
    }

    /**
      * @Description: 维修记录
      * @Author zys
      * @Date 2020/7/1 0001 4:38
      * @Param [map]
     */
    @Override
    public Map listRepair(Map map) {
        PageUtil.list(map);
        PageInfo<Map> pageInfo = new PageInfo(caruseDao.listRepair(map));
        Map repairMap=new HashMap();
        repairMap.put("total",pageInfo.getTotal());
        repairMap.put("page",pageInfo.getList());
        return repairMap;
    }

}
