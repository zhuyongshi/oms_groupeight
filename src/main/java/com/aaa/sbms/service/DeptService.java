package com.aaa.sbms.service;

import java.util.List;
import java.util.Map;

public interface DeptService {
    /**
      * @Description: 获取部门信息
      * @Author zys
      * @Date 2020/6/12 0012 15:49
      * @Param []
      * @return java.util.List<java.util.Map>
     */
    List<Map> listDept();
    /**
      * @Description: 获取部门信息
      * @Author zys
      * @Date 2020/6/15 0015 22:14
      * @Param [map]
      * @return java.util.List<java.util.Map>
     */
    Map getDeptList(Map map);
    /**
      * @Description:更新部门数据
      * @Author zys
      * @Date 2020/6/16 0016 13:11
      * @Param [map]
      * @return java.util.List<java.util.Map>
     */
    int update(Map map);

    /**
      * @Description:添加部门信息
      * @Author zys
      * @Date 2020/6/16 0016 14:10
      * @Param [map]
      * @return int
     */
    int add(Map map);

    /**
      * @Description:删除部门
      * @Author zys
      * @Date 2020/6/16 0016 14:34
      * @Param [deptId]
      * @return int
     */
    int delete(Integer deptId);
}
