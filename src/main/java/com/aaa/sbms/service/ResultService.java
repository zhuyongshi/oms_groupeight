package com.aaa.sbms.service;


import java.util.List;
import java.util.Map;

/**
 * 考核结果
 * @author 10744
 */
public interface ResultService {

    Map getAll(Map map);

    List<Map> getForm();

    int update(Map map);
}
