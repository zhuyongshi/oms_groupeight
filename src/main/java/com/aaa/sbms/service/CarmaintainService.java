package com.aaa.sbms.service;

import java.util.Map;

public interface CarmaintainService {
    /**
     * 查询所有
     * @param map
     * @return
     */
    Map selectAllMain(Map map);

    /**
     * 删除
     * @param mainid
     * @return
     */
    int deleteMainId(Integer mainid);

    /**
     * 添加
     * @param map
     * @return
     */
    int addMain(Map map);

    /**
     * 更新
     * @param map
     * @return
     */
    int updateMain(Map map);

    /**
      * @Description: 车辆报修
      * @Author zys
      * @Date 2020/7/1 0001 2:19
      * @Param [map]
     */
    int addMaintain(Map map);

    /**
      * @Description: 维修
      * @Author zys
      * @Date 2020/7/1 0001 2:56
      * @Param [map]
     */
    int toMaintain(Map map);
}
