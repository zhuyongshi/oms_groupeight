package com.aaa.sbms.service;

import java.util.List;
import java.util.Map;

public interface CarService {

    /**
     * 查询车辆信息
     * @param map
     * @return
     */
    Map selectAllCar(Map map);

    /**
     * 根据id删除车辆
     * @param carid
     * @return
     */
    int deleteCarId(Integer carid);

    /**
     * 添加
     * @param map
     * @return
     */
    int addCar(Map map);

    /**
     * 更新车辆信息
     * @param map
     * @return
     */
    int updataCar(Map map);


    /**
      * @Description: 获取可用车辆
      * @Author zys
      * @Date 2020/6/30 0030 21:44
      * @Param []
     */
    List<Map> getUserCar();

    /**
      * @Description: 申请用车
      * @Author zys
      * @Date 2020/6/30 0030 21:57
      * @Param [map]
     */
    int toUseCar(Map map);
}
