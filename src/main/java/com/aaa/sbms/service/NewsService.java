package com.aaa.sbms.service;



import java.util.List;
import java.util.Map;

/**
 * fileName:NewsService
 * description:
 * author:zz
 * createTime:2019/11/26 14:14
 * version:1.0.0
 */
public interface NewsService {

    /**
     * 新闻列表
     * @return
     */
    List<Map> getList();

    /**
     *
     * @param map
     * @return
     */
     Map  getPage(Map map);

    /**
     * 添加
     * @param map
     * @return
     */
    int add(Map map);

    /**
     * 更新
     * @param map
     * @return
     */
    int update(Map map);

    /**
     * 根据编号删除
     * @param id
     * @return
     */
    int delete(int id);
}
