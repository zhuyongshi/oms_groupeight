package com.aaa.sbms.service;

import java.util.List;
import java.util.Map;

public interface CheckOutService {

    /**
      * @Description: 获取退宿列表
      * @Author zys
      * @Date 2020/6/23 0023 14:50
      * @Param [map]
     */
    Map listCheckOut(Map map);

   /**
     * @Description: 办理退宿
     * @Author zys
     * @Date 2020/6/23 0023 15:15
     * @Param [id]
    */
    int toCheckOut(Map map);
}
