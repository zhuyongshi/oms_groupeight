package com.aaa.sbms.service;

import java.util.Map;

public interface RepairService {
    /**
      * @Description: 获取修理信息
      * @Author zys
      * @Date 2020/6/23 0023 16:44
      * @Param [map]
     */
    Map listRepair(Map map);

    /**
      * @Description: 维修处理
      * @Author zys
      * @Date 2020/6/23 0023 17:21
      * @Param [map]
     */
    int toHandle(Map map);
}
