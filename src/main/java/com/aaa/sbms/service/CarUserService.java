package com.aaa.sbms.service;

import java.util.Map;

public interface CarUserService {
    /**
     * 查询所有
     * @param map
     * @return
     */
    Map selectAllUse(Map map);

    /**
      * @Description: 维修记录
      * @Author zys
      * @Date 2020/7/1 0001 4:37
      * @Param [map]
     */
    Map listRepair(Map map);
}
