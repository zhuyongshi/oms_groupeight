package com.aaa.sbms.service;


import com.aaa.sbms.entity.Emp;

import java.util.List;
import java.util.Map;

/**
 * fileName:UserService
 * description:
 * author:zz
 * createTime:2019/11/26 16:38
 * version:1.0.0
 */
public interface UserService {

    /**
     * 功能描述
     * @Author zys
     * @Description 获取员工信息
     * @Date 2020/6/11 0011 14:33
     * @Param []
     * @return java.util.List<java.util.Map>
     */
    Map getEmpList(Map map);
    /**
      * @Description:
      * @Author zys
      * @Date 2020/6/12 0012 16:41
      * @Param []
      * @return java.util.List
     */
    //List<Map> listENames();

    /**
     * 根据用户名查询用户信息（登录使用）
     * @param userName
     * @return
     */
    Map  getUserByUserName(String userName);
    /**
     * 根据条件查询
     * @param map
     * @return
     */
    Map getList(Map map);
    /***
     * 添加
     * @param map
     * @return
     */
    int add(Map map);

    /**
     * 更新
     * @param map
     * @return
     */
    int update(Map map);

    /**
     * 删除
     * @param userId
     * @return
     */
    int delete(int userId);

    /**
     * 查询用户id对应的角色id集合
     * @param userId
     * @return
     */
    List<Integer>  listRoleIdsByUserId(int userId);
    /**
     * 删除原有用户ID关联的所有角色
     * @param userId
     * @return
     */
    int delteRoleIdsByUserId(int userId);
    /**
     * 添加用户角色关联
     * @param userId
     * @param roleIds
     * @return
     */
    int addUserAndRole(int userId,String roleIds);
    /**
      * @Description: 获取领导
      * @Author zys
      * @Date 2020/6/12 0012 19:32
      * @Param [deptId]
      * @return java.util.List<java.util.Map>
     */
    List<Map> getLeader(int deptId);

    /**
      * @Description: 修改用户头像
      * @Author zys
      * @Date 2020/6/16 0016 21:27
      * @Param [imageUrl]
      * @return int
     */
    int updateAvatar(String imageUrl);

    /**
      * @Description: 获取头像
      * @Author zys
      * @Date 2020/6/16 0016 22:51
      * @Param []
      * @return java.util.List<java.util.Map>
     */
    Map getPerson();

    /**
      * @Description:更新个人信息
      * @Author zys
      * @Date 2020/6/17 0017 15:21
      * @Param [map]
      * @return int
     */
    int updatePerson(Map map);

    /**
      * @Description: 更新密码
      * @Author zys
      * @Date 2020/6/17 0017 16:07
      * @Param [map]
      * @return int
     */
    int updatePwd(String password);

    /**
      * @Description:删除用户角色
      * @Author zys
      * @Date 2020/6/17 0017 19:08
      * @Param [user_id]
      * @return int
     */
    int delRole(Integer user_id);

    /**
      * @Description: 获取员工信息
      * @Author zys
      * @Date 2020/6/26 0026 11:38
      * @Param []
     */
    List<Emp> getAllEmp();

}
