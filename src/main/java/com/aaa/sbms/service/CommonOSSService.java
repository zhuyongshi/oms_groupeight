package com.aaa.sbms.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * fileName:CommonOSSService
 * description:
 * author:zz
 * createTime:2020/3/20 15:03
 * version:1.0.0
 */
public interface CommonOSSService {
    /**
     * 上传文件至阿里云 oss
     *
     * @param file  上传的源文件
     * @return  返回值ossFileUrlBoot oss地址  oldFileName 源文件名称
     * @throws Exception
     */
    Map uploadOSS(MultipartFile file) throws Exception;
}
