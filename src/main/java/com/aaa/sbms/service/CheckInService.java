package com.aaa.sbms.service;

import java.util.List;
import java.util.Map;

public interface CheckInService {
    /**
      * @Description: 获取入住员工信息
      * @Author zys
      * @Date 2020/6/22 0022 15:17
      * @Param [map]
     */
    Map listCheckIn(Map map);

    /**
      * @Description:修改入住意向
      * @Author zys
      * @Date 2020/6/22 0022 16:03
      * @Param [userId]
     */
    int changeIsLock(Integer userId,Integer isLock);

    /**
      * @Description: 根据性别获取宿舍地址
      * @Author zys
      * @Date 2020/6/22 0022 16:52
      * @Param [sex]
     */
    List<Map> listHouses(Integer sex);

    /**
      * @Description: 根据宿舍获取楼层
      * @Author zys
      * @Date 2020/6/22 0022 17:22
      * @Param [houseId]
     */
    List<Map> getFloors(Integer houseId);

    /**
      * @Description: 根据楼层获取房间
      * @Author zys
      * @Date 2020/6/22 0022 17:49
      * @Param [houseId, floor]
     */
    List<Map> getRooms(Map map);

    /**
      * @Description: 根据房间号获取床位号
      * @Author zys
      * @Date 2020/6/22 0022 22:09
      * @Param [map]
     */
    List<Map> getBeds(Map map);

    /**
      * @Description: 添加入住信息
      * @Author zys
      * @Date 2020/6/22 0022 22:22
      * @Param [map]
     */
    int checkInInfo(Map map);
}
