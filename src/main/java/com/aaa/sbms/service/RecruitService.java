package com.aaa.sbms.service;

import java.util.Map;

public interface RecruitService {
    /**
      * @Description: 获取招聘信息
      * @Author zys
      * @Date 2020/6/25 0025 11:10
      * @Param [map]
     */
    Map listInfo(Map map);

    /**
      * @Description: 更新招聘信息状态
      * @Author zys
      * @Date 2020/6/25 0025 12:58
      * @Param [id]
     */
    int updateStateById(Integer id);

    /**
      * @Description: 更新信息
      * @Author zys
      * @Date 2020/6/25 0025 13:25
      * @Param [map]
     */
    int update(Map map);

    /**
      * @Description: 添加信息
      * @Author zys
      * @Date 2020/6/25 0025 13:27
      * @Param [map]
     */
    int add(Map map);

    /**
      * @Description: 入职审核记录
      * @Author zys
      * @Date 2020/6/25 0025 14:15
      * @Param [map]
     */
    Map recordList(Map map);

    /**
      * @Description: 获取待审核应聘信息
      * @Author zys
      * @Date 2020/6/25 0025 15:43
      * @Param [map]
     */
    Map listCheck(Map map);

    /**
      * @Description: 审批
      * @Author zys
      * @Date 2020/6/25 0025 16:00
      * @Param [map]
     */
    int pass(Map map);

    /**
      * @Description: 驳回申请
      * @Author zys
      * @Date 2020/6/25 0025 16:07
      * @Param [map]
     */
    int unPass(Map map);

    /**
      * @Description: 入职新员工
      * @Author zys
      * @Date 2020/6/25 0025 16:28
      * @Param [map]
     */
    Map newList(Map map);

    /**
      * @Description: 驳回新员工入职
      * @Author zys
      * @Date 2020/6/25 0025 17:27
      * @Param [map]
     */
    int leave(Integer id);

    /**
      * @Description: 入职新员工
      * @Author zys
      * @Date 2020/6/25 0025 18:00
      * @Param [map]
     */
    int addEmp(Map map);

    /**
      * @Description: 添加简历
      * @Author zys
      * @Date 2020/6/28 0028 21:21
      * @Param [map]
     */
    int addResume(Map map);
}
