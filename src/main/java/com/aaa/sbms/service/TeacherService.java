package com.aaa.sbms.service;

import com.aaa.sbms.entity.Teacher;

import java.util.List;
import java.util.Map;

public interface TeacherService {
    Map selectAllTeacher(Map map);

    int deleteTeacherById(int teacherId);

    int addTeacher(Map map);

    int updateTeacher(Map map);

    int deleteDatas(List<Integer> list);

    Map getName();

    List<Teacher> getAllTeacher();
}
