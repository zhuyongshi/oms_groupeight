package com.aaa.sbms.service;

import java.util.Map;

public interface QuitService {

    /**
      * @Description: 获取离职记录
      * @Author zys
      * @Date 2020/6/24 0024 15:16
      * @Param [map]
     */
    Map listQuit(Map map);

    /**
      * @Description: 获取待审核离职信息
      * @Author zys
      * @Date 2020/6/24 0024 17:00
      * @Param [map]
     */
    Map listsRecord(Map map);

    /**
      * @Description: 通过审批
      * @Author zys
      * @Date 2020/6/24 0024 17:26
      * @Param [map]
     */
    int updateCheckState(Map map);

    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/24 0024 17:32
      * @Param [map]
     */
    int changeCheckState(Map map);

    /**
      * @Description: 提交离职申请
      * @Author zys
      * @Date 2020/6/29 0029 13:30
      * @Param [map]
     */
    int toLeave(Map map);
}
