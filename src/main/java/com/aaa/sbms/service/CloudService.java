package com.aaa.sbms.service;

import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Map;

/**
 * 网盘
 * @author Fong
 */
public interface CloudService {

    /**
     * 上传文件
     * @param userId
     * @param file
     * @return int
     */
    int upload(int userId, MultipartFile file);

    /**
     * 文件列表
     * @param userId
     * @return
     */
    List<Map> getFileList(int userId);

    /**
     * 下载文件
     * @param fileId
     * @return
     */
    Map getFileByid(int fileId);

    /**
     * 更新文件名
     * @param map
     * @return
     */
    int updateFileName(Map map);

    /**
     * 删除文件（移到回收站）
     * @param fileId
     * @return
     */
    int deleteFile(Integer fileId);
}
