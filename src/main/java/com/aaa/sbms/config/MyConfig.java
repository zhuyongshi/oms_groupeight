package com.aaa.sbms.config;

import com.aaa.sbms.entity.Dept;
import com.aaa.sbms.entity.Emp;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * fileName:MyConfig
 * description:
 * author:zz
 * createTime:2019/11/26 15:48
 * version:1.0.0
 */
//@Configuration
public class MyConfig {

    /**
     * 相当于过去的
     *  <bean name="emp"   class="com.aaa.sbms.entity.Emp"  >
     * @return
     */
    /*@Bean
    public Emp emp(@Qualifier("d1") Dept d){
        Emp emp = new Emp();
        emp.setEname("zhangsan");
        emp.setSal(10000);
        emp.setDept(d);
        return emp;
    }*/

    @Bean("d1")
    public Dept dept(){
        Dept dept =new Dept();
        dept.setDname("kaifa1");
        dept.setLoc("1lou");
        return dept;
    }
}
