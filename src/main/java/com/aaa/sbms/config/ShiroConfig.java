package com.aaa.sbms.config;

import com.aaa.sbms.util.MyShiroRealm;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * fileName:ShiroConfig
 * description:
 * author:zz
 * createTime:2019/11/26 16:06
 * version:1.0.0
 */
@Configuration  //用jav代替过去的spring xml文件
public class ShiroConfig {

    /**
     * 拦截配置工厂
     * @return
     */
    @Bean
    public ShiroFilterFactoryBean shiroFilter(){
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager());
        //未登录（认证）跳转页面
        shiroFilterFactoryBean.setLoginUrl("/html/login.html");
        //成功后跳转页面和未授权跳转页面，没有用，暂时不设置
        //shiroFilterFactoryBean.setUnauthorizedUrl("");
        //shiroFilterFactoryBean.setSuccessUrl();
        //因为要把/**= authc 放在最下面，如果使用HashMap 就无序了
        Map map  =new LinkedHashMap();
        //需要放开的请求
        //静态资源
        map.put("/css/**","anon");
        map.put("/imgs/**","anon");
        map.put("/js/**","anon");
        //开放前台
        map.put("/html/front.html","anon");
        //开发投简历请求
        map.put("/html/font/join.html","anon");
        //获取招聘信息
        map.put("/recruit/page","anon");
        //登录相关
        map.put("/html/login.html","anon");
        map.put("/user/login","anon");
        //注销
        map.put("/logout","logout");
        //实际做项目过程中，放开的更多
        //除了放开的全部拦截
        map.put("/**","authc");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(map);
        return shiroFilterFactoryBean;
    }

    /**
     * 配置shiro的核心组件  securityManager
     * @return
     */
    @Bean
    public DefaultWebSecurityManager  securityManager(){
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(myShiroRealm());
        return defaultWebSecurityManager;
    }

    /**
     * 配置安全数据桥梁
     * @return
     */
    @Bean
    public MyShiroRealm myShiroRealm(){
        MyShiroRealm myShiroRealm =new MyShiroRealm();
        myShiroRealm.setCredentialsMatcher(credentialsMatcher());
        return myShiroRealm;
    }

    /**
     * 加密
     * @return
     */
    @Bean
    public HashedCredentialsMatcher credentialsMatcher(){
        HashedCredentialsMatcher hashedCredentialsMatcher =new HashedCredentialsMatcher();
        //加密方式
        hashedCredentialsMatcher.setHashAlgorithmName("SHA-512");
       // hashedCredentialsMatcher.set
        //哈希次数
        hashedCredentialsMatcher.setHashIterations(1024);
        return hashedCredentialsMatcher;
    }

}
