package com.aaa.sbms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("com.aaa.sbms.dao")
@EnableTransactionManagement  //开启注解式事务
@Configuration
public class SbmHFYBMSApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbmHFYBMSApplication.class, args);
    }
}
