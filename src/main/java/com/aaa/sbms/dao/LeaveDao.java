package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface LeaveDao {
    /**
      * @Description: listFirstRecord
      * @Author zys
      * @Date 2020/6/29 0029 20:27
      * @Param [map]
     */
    @Select("<script>select l.*,u.username from sys_leavelist l \n" +
            "left join sys_user u on l.workId=u.workId where l.firstcheck =0 \n" +
            "<if test=\"userName!=null and userName!=''\"> and u.username like concat('%',#{userName},'%')</if></script>")
    List<Map> listFirstRecord(Map map);

    /**
      * @Description: 批准
      * @Author zys
      * @Date 2020/6/29 0029 20:59
      * @Param [map]
     */
    @Update("update sys_leavelist set firstcheck=1, fsuggest=#{fsuggest} where id=#{id}")
    int toPass(Map map);

    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/29 0029 21:05
      * @Param [map]
     */
    @Update("update sys_leavelist set firstcheck=2, fsuggest=#{fsuggest},checkId=#{checkId} where id=#{id}")
    int toUnpass(Map map);

    /**
      * @Description: 获取审核记录信息
      * @Author zys
      * @Date 2020/6/29 0029 21:21
      * @Param [map]
     */
    @Select("<script>select l.*,u.username from sys_leavelist l \n" +
            "left join sys_user u on l.workId=u.workId where l.firstcheck =1 or  l.firstcheck=2 \n" +
            "<if test=\"userName!=null and userName!=''\"> and u.username like concat('%',#{userName},'%')</if></script>")
    List<Map> listHistoryList(Map map);

    /**
      * @Description: 获取领导审核信息
      * @Author zys
      * @Date 2020/6/29 0029 21:40
      * @Param [map]
     */
    @Select("<script>select l.*,u.username from sys_leavelist l \n" +
            "left join sys_user u on l.workId=u.workId where l.firstcheck =1 and twocheck=0\n" +
            "<if test=\"userName!=null and userName!=''\"> and u.username like concat('%',#{userName},'%')</if></script>")
    List<Map> listTwoRecord(Map map);

    /**
      * @Description: 二次批准
      * @Author zys
      * @Date 2020/6/29 0029 21:44
      * @Param [map]
     */
    @Update("update sys_leavelist set twocheck=1, tsuggest=#{tsuggest},checkId=#{checkId} where id=#{id}")
    int toTowPass(Map map);

    /**
      * @Description: 二次驳回
      * @Author zys
      * @Date 2020/6/29 0029 21:47
      * @Param [map]
     */
    @Update("update sys_leavelist set twocheck=2, tsuggest=#{tsuggest} where id=#{id}")
    int toTowUnPass(Map map);

    /**
      * @Description: 请假
      * @Author zys
      * @Date 2020/6/30 0030 16:44
      * @Param [map]
     */
    @Insert("insert into sys_leavelist values(null,#{workId},#{urge},#{leaveType},#{num},#{beginTime},#{endTime},\n" +
            "now(),#{reason},0,0,null,null,#{tel},null)")
    int toAddLeave(Map map);

    /**
      * @Description: 查询结果
      * @Author zys
      * @Date 2020/6/30 0030 17:53
      * @Param []
     */
    @Select("select firstcheck, twocheck from sys_leavelist where  workId=#{workId}")
    Map checkResult(String workId);

}
