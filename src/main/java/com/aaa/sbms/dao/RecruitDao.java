package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface RecruitDao {
    /**
      * @Description: 获取招聘信息
      * @Author zys
      * @Date 2020/6/25 0025 11:13
      * @Param [map]
     */
    @Select("<script>select r.*,d.dname dName from sys_recruitinfo r left join dept d on r.dId=d.deptId where state=0\n" +
            "<if test=\"dId!=null and dId!=''\"> and r.dId=#{dId}</if></script>")
    List<Map> listInfo(Map map);

    /**
      * @Description: 更新招聘信息状态
      * @Author zys
      * @Date 2020/6/25 0025 12:59
      * @Param [id]
     */
    @Update("update sys_recruitinfo set state=1 where id=#{id}")
    int updateStateById(Integer id);

    /**
      * @Description: 更新信息
      * @Author zys
      * @Date 2020/6/25 0025 13:26
      * @Param [map]
     */
    @Update("update sys_recruitinfo set dId=#{dId},position=#{position},num=#{num},\n" +
            "requite=#{requite},duty=#{duty},treat=#{treat} where id=#{id}")
    int update(Map map);

    /**
      * @Description:
      * @Author zys
      * @Date 2020/6/25 0025 13:27
      * @Param [map]
     */
    @Insert("insert into sys_recruitinfo " +
            "values(null,#{dId},#{position},#{num},#{requite},#{duty},#{treat},0)")
    int add(Map map);

    /**
      * @Description: 入职审核记录
      * @Author zys
      * @Date 2020/6/25 0025 14:17
      * @Param [map]
     */
    @Select("<script>select r.*,d.dname dName from sys_recruit_record r \n" +
            "left join  dept d on r.dId=d.deptId where state=0\n" +
            "<if test=\"result!=null and result!=''\"> and r.result=#{result}</if> order by r.pId</script>")
    List<Map> recordList(Map map);

    /**
      * @Description: 获取待审核应聘信息
      * @Author zys
      * @Date 2020/6/25 0025 15:45
      * @Param [map]
     */
    @Select("<script>select r.*,d.dname dName from sys_recruit_record r \n" +
            "left join  dept d on r.dId=d.deptId where state=1\n" +
            "<if test=\"userName!=null and userName!=''\"> and r.pName  like concat('%',#{userName},'%')</if> order by r.pId</script>")
    List<Map> listCheck(Map map);

    /**
      * @Description: 审批
      * @Author zys
      * @Date 2020/6/25 0025 16:00
      * @Param [map]
     */
    @Update("update sys_recruit_record set reason=#{reason},state=0,time=now(),result=0 where pId=#{pId}")
    int pass(Map map);

    /**
      * @Description:驳回申请
      * @Author zys
      * @Date 2020/6/25 0025 16:08
      * @Param [map]
     */
    @Update("update sys_recruit_record set reason=#{reason},state=0,time=now(),result=1 where pId=#{pId}")
    int unPass(Map map);

    /**
      * @Description: 添加新员工
      * @Author zys
      * @Date 2020/6/25 0025 16:29
      * @Param [map]
     */

    @Select("<script>select r.*,d.dname dName from sys_recruit_record r \n" +
            "left join  dept d on r.dId=d.deptId where state=0 and result=0 \n" +
            "<if test=\"userName!=null and userName!=''\"> and r.pName  like concat('%',#{userName},'%')</if> order by r.pId</script>")
    List<Map> newList(Map map);

    /**
      * @Description: 驳回新员工入职
      * @Author zys
      * @Date 2020/6/25 0025 17:28
      * @Param [map]
     */
    @Update("update sys_recruit_record set result=1 where pId=#{id}")
    int leave(Integer id);

    /**
      * @Description: 入职新员工
      * @Author zys
      * @Date 2020/6/25 0025 18:01
      * @Param [map]
     */
    @Insert("insert into sys_user (username,password,salt,status,workId,tel,\n" +
            "sex,isLock,lockDay,groupId,idCard) \n" +
            "values(#{pName},#{password},#{salt},0,#{workId},#{tel},\n" +
            "#{sex},0,now(),#{dId},#{idCard})")
    int addEmp(Map map);

    /**
      * @Description: 修改入职状态
      * @Author zys
      * @Date 2020/6/25 0025 18:12
      * @Param [map]
     */
    @Update("update sys_recruit_record set result=2 where pId=#{pId}")
    int changeResult(Map map);

    /**
      * @Description: 添加简历
      * @Author zys
      * @Date 2020/6/28 0028 21:22
      * @Param [map]
     */
    @Insert("insert into sys_recruit_record \n" +
            "values(null,#{pName},#{sex},#{graduation},#{education},#{dId},\n" +
            "#{position},#{tel},#{idCard},now(),#{time},#{result},#{reason},1)")
    int addResume(Map map);
}
