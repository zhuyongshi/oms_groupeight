package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface RepairDao {

    /**
      * @Description: 获取修理信息
      * @Author zys
      * @Date 2020/6/23 0023 16:46
      * @Param [map]
     */
    @Select("<script>select h.*,hi.houseId,hi.floor,hi.room,hi.bed,hi.status,hs.address,u.username\n" +
            "from sys_houserepaire h left join sys_user u on h.userid=u.workId\n" +
            "left join sys_houses_items hi on h.hId = hi.id \n" +
            "left join sys_houses hs on hi.houseId=hs.houseId where state=0 \n" +
            "<if test=\"userName!=null and userName!=''\"> and username like concat('%',#{userName},'%')</if></script>")
    List<Map> listRepair(Map map);

    /**
      * @Description: 处理维修
      * @Author zys
      * @Date 2020/6/23 0023 17:22
      * @Param
     */
    @Update("update sys_houserepaire set state=1 where id=#{id}")
    int toHandle(Map map);

    /**
      * @Description: 修改房间处理状态
      * @Author zys
      * @Date 2020/6/23 0023 17:24
      * @Param [map]
     */
    @Update("update sys_houses_items set status=0 where id=#{hId}")
    int updateRoomsStatus(Map map);
}
