package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * 课程管理Dao
 * Fong
 */
public interface CoursesDao {

    /**
     * 查询所有课程
     * @param map
     * @return
     */
    @Select("<script> select c_id,c_name,c_info,c_status,c_number,create_user,date_format(create_time,'%Y-%m-%d %H:%i:%S' ) create_time,update_user,date_format(update_time,'%Y-%m-%d %H:%i:%S') update_time from sys_course"+
            "<if test=\"cName!=null and cName!=''\"> where c_name like concat('%',#{cName},'%')</if>" +
            "</script>")
    List<Map> getCourseAll(Map map);

    /**
     * 根据ID删除课程
     * @param courseId
     * @return
     */
    @Delete("delete from sys_course where c_id=#{courseId}")
    int deleteCourseById(int courseId);

    /**
     * 添加课程信息
     * @param map
     * @return
     */
    @Insert("INSERT INTO sys_course ( c_name, c_info, create_user, create_time,update_user,update_time )" +
            "VALUES" +
            "(#{c_name},#{c_info},#{userName},now(),#{userName},now());")
    int addCourse(Map map);

    /**
     * 编辑课程信息
     * @param map
     * @return
     */
    @Update("update sys_course set c_name=#{c_name},  c_info=#{c_info},update_user=#{userName}, update_time=now() where c_id=#{c_id} ")
    int updataCourse(Map map);

    @Select("select c_id,c_name from sys_course")
    List<Map> getCourseName();

    @Update("update sys_course set c_number=#{number} where c_name=#{name}")
    int changeCourseNumber(String name,Integer number);

    @Select("select c_number from sys_course where c_name=#{name}")
    Map getNumber(String name);
}
