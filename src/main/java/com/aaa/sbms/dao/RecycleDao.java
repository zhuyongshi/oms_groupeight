package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * 回收站
 * @author 10744
 */
public interface RecycleDao {

    /**
     * 查询文件
     * @param userId
     * @return
     */
    @Select("select f_id, f_name,date_format(delete_time,'%Y-%m-%d')delete_time from sys_file where user_id=#{userId} and f_status = 2")
    List<Map> getFileList(int userId);

    /**
     * 彻底删除
     * @param fileId
     * @return
     */
    @Delete("delete from sys_file where f_id=#{fileId} and f_status = 2")
    int deleteFile(Integer fileId);

    /**
     * 还原文件到我的文件
     * @param fileId
     * @return
     */
    @Update("update sys_file set f_status=1 where f_id=#{fileId} and f_status = 2")
    int rollback(Integer fileId);
}
