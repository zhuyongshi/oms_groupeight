package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * 课程计划
 * Fong
 */
public interface CoursePlanDao {

    /**
     * 查询所有
     * @param map
     * @return
     */
    @Select("<script> select cp_id,cp_cname,cp_name,date_format( cp_startTime,'%Y-%m-%d') cp_startTime, date_format( cp_endTime,'%Y-%m-%d') cp_endTime,"+
            "date_format(cp_startTime,'%H:%i')cp_classTime,cp_time,cp_status,create_user,date_format(create_time,'%Y-%m-%d %H:%i:%S')create_time,"+
            "updata_user,date_format(updata_time,'%Y-%m-%d %H:%i:%S')updata_time,t_name from sys_courseplan cp"+
            "<if test=\"cpName!=null and cpName!=''\"> where cp_name like concat('%',#{cpName},'%')</if>" +
            "</script>")
    List<Map> getCoursePlanAll(Map map);

    /**
     * 根据ID删除课程计划
     * @param coursePlanId
     * @return
     */
    @Delete("delete from sys_courseplan where cp_id=#{coursePlanId}")
    int deleteCoursePlanById(int coursePlanId);

    /**
     * 添加课程计划
     * @param map
     * @return
     */
    @Insert("INSERT INTO sys_courseplan ( cp_cname, cp_name,cp_startTime,cp_endTime, cp_time, t_name, create_user, create_time, updata_user, updata_time,cp_status )" +
            "VALUES" +
            "(#{cp_cname},#{cp_name},#{cp_startTime},#{cp_endTime},#{cp_time},#{t_name},#{userName},now(),#{userName},now(),2)")
    int addCoursePlan(Map map);

    /**
     * 更新课程计划
     * @param map
     * @return
     */
    @Update("update sys_courseplan set cp_cname=#{cp_cname},t_name=#{t_name},cp_name=#{cp_name},cp_startTime=#{cp_startTime},cp_endTime=#{cp_endTime},cp_status=#{cp_status},cp_time=#{cp_time},updata_user=#{userName},updata_time=now() where cp_id=#{cp_id}")
    int updateCoursePlan(Map map);

    @Update("update sys_courseplan set cp_status=#{status} where t_name=#{name}")
    int changeCoursePlanStatus(String name,Integer status);

    @Select("select cp_cname from sys_courseplan where t_name=#{name}")
    Map getCourse(String name);


}
