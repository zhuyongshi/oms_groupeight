package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface CarDao {
    /**
     * 查询所有
     * @param map
     * @return
     */
    @Select("<script> select carid,carbyte,numplate,carnumber,oilmass,carstate,u.username,\n" +
            "damagelevel from sys_car join sys_user u where sys_car.user_id = u.user_id " +
            "<if test=\"carbyte!=null and carbyte!=''\"> and carbyte like concat('%',#{carbyte},'%')</if>" +
            "<if test=\"numplate!=null and numplate!=''\"> and numplate like concat('%',#{numplate},'%')</if></script>")
    List<Map> selectAllCat(Map map);

    /**
     * 根据id删除信息
     * @param carid
     * @return
     */
    @Delete("delete from sys_car where carid=#{carid}")
    int deleteCarId(Integer carid);

    /**
     *添加
     * @param map
     * @return
     */
    @Insert("insert into sys_car (carbyte,numplate,carnumber,oilmass,carstate,user_id,damagelevel) values (#{carbyte},#{numplate},#{carnumber},#{oilmass},#{carstate},#{user_id},#{damagelevel})")
    int addCar(Map map);

    /**
     * 更新车辆信息
     * @param map
     * @return
     */
    @Update("update sys_car set carbyte=#{carbyte},numplate=#{numplate},carnumber=#{carnumber},oilmass=#{oilmass},carstate=#{carstate},user_id=#{user_id},damagelevel=#{damagelevel} where carid=#{carid}")
    int updateCar(Map map);

    /**
     * 通过id查询用户id和名称
     * @return
     */
    @Select("select distinct u.user_id,u.username from sys_user u " +
            "left join sys_car c on c.user_id = u.user_id ")
    List<Map> getUser();

    /**
      * @Description:
      * @Author zys
      * @Date 2020/6/30 0030 21:45
      * @Param []
     */
    @Select("select * from sys_car where carstate=1")
    List<Map> getUserCar();

    /**
      * @Description: 申请用车
      * @Author zys
      * @Date 2020/6/30 0030 21:58
      * @Param [map]
     */
    @Insert("insert into sys_car_audit \n" +
            "values(null,#{user_id},#{starttime},null,#{userason},0,null,#{carid},null,0)")
    int toUseCar(Map map);
}
