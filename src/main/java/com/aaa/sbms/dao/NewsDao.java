package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * fileName:NewsDao
 * description:
 * author:zz
 * createTime:2019/11/26 14:12
 * version:1.0.0
 */
public interface NewsDao {

    /**
     * 新闻列表
     * @return
     */
    @Select("select * from tb_news")
    List<Map>  getList();

    /**
     * 带参分页查询
     * @param map
     * @return
     */
    @Select("<script>select * from (\n" +
            "  select rownum rn,t.* from (\n" +
            "   select * from tb_news  <where>" +
            " <if test='title!=null'> and title like '%${title}%' </if>"+
            " <if test=\"clickNum!=null and clickNum !=''\"> and clicknum =#{clickNum} </if>"+
            " <if test=\"typeId!=null and typeId != ''\"> and type_id =#{typeId} </if>"+
            "</where> order by id desc\n" +
            "  ) t where    rownum &lt; #{end}  \n" +
            ") where rn &gt; #{begin}    </script>")
    List<Map>  getPage(Map map);

    /**
     * 带参总数量查询
     * @param map
     * @return
     */
    @Select("<script>select count(*) from tb_news  <where>" +
            " <if test='title!=null'> and title like '%${title}%' </if>"+
            " <if test=\"clickNum!=null and clickNum !=''\"> and clicknum =#{clickNum} </if>"+
            " <if test=\"typeId!=null and typeId != ''\"> and type_id =#{typeId} </if>"+
            "</where></script>")
    int getPageCount(Map map);

    /**
     * 添加
     * @param map
     * @return
     */
    @Insert("insert into tb_news values(seq_news_id.nextval,#{TITLE},#{CLICKNUM},sysdate,#{TYPE_ID})")
    int add(Map map);

    /**
     * 更新
     * @param map
     * @return
     */
    @Update("update tb_news set title = #{TITLE},clicknum = #{CLICKNUM},type_id =#{TYPE_ID} where id=#{ID}")
    int update(Map map);

    /**
     * 根据编号删除
     * @param id
     * @return
     */
    @Delete("delete from tb_news where id=#{id}")
    int delete(int id);
}
