package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface DeptDao {
    /**
      * @Description: 获取全部部门信息
      * @Author zys
      * @Date 2020/6/12 0012 15:48
      * @Param []
      * @return java.util.List<java.util.Map>
     */
    @Select("select d.*,u.username ,d.deptId dId from dept d left join sys_user u on d.uWorkId=u.workId")
    List<Map> listDept();

    /**
      * @Description: 获取部门信息
      * @Author zys
      * @Date 2020/6/15 0015 22:15
      * @Param [map]
      * @return java.util.List<java.util.Map>
     */
    @Select("<script>select d.deptId,d.dname,d.remark,d.dstatus,d.uWorkId,u.username from dept d " +
            "left join sys_user u on d.uWorkId=u.workId where d.dstatus=1\n" +
            "<if test=\"dname!=null and dname!=''\"> and d.dname like concat('%',#{dname},'%')</if></script>")
    List<Map>getDeptList(Map map);
    /**
      * @Description:更新部门信息
      * @Author zys
      * @Date 2020/6/16 0016 13:16
      * @Param [map]
      * @return java.util.List<java.util.Map>
     */
    @Update("update dept set dname=#{dname},uWorkId=#{uWorkId},remark=#{remark} where deptId=#{deptId}")
    int update(Map map);
    /**
      * @Description:获取部门老大
      * @Author zys
      * @Date 2020/6/16 0016 13:33
      * @Param []
      * @return java.util.List<java.util.Map>
     */
    @Select("select u.username,u.workId from sys_user u " +
            "left join dept d on d.deptId=u.groupId where d.deptId=1")
    List<Map> getLeaders();

    /**
      * @Description:添加部门信息
      * @Author zys
      * @Date 2020/6/16 0016 14:16
      * @Param [map]
      * @return int
     */
    @Insert("insert into dept values(null,#{dname},now(),#{dstatus},#{uWorkId},#{remark})")
    int add(Map map);

    /**
      * @Description:删除部门
      * @Author zys
      * @Date 2020/6/16 0016 14:35
      * @Param [deptId]
      * @return int
     */
    @Update("update dept set dstatus=2 where deptId=#{deptId}")
    int delete(Integer deptId);
}
