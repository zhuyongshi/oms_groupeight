package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface CheckOutDao {

    /**
      * @Description: 获取退宿列表
      * @Author zys
      * @Date 2020/6/23 0023 14:51
      * @Param [map]
     */
    @Select("<script>select c.*,h.address from sys_checkin c left join sys_houses h on c.houseId=h.houseId where state=0 " +
            "<if test=\"userName!=null and userName!=''\"> and username like concat('%',#{userName},'%')</if></script>")
    List<Map> listCheckOut(Map map);

    /**
      * @Description: 办理退宿
      * @Author zys
      * @Date 2020/6/23 0023 15:17
      * @Param [id]
     */
    @Update("update sys_checkin set checkOutDate=now(),state=1 where id=#{id}")
    int toCheckOut(Map map);

    /**
      * @Description: 修改床位状态
      * @Author zys
      * @Date 2020/6/23 0023 15:26
      * @Param [map]
     */
    @Update("update sys_bed set status=0 where houseId=#{houseId} and floorId=#{floor} and roomId=#{room} and bedId=#{bedId}")
    int updateBedStatus(Map map);

    /**
      * @Description: 修改房间状态
      * @Author zys
      * @Date 2020/6/23 0023 15:40
      * @Param [map]
     */
    @Update("update sys_houses_items set status=0 where houseId=#{houseId} and floor=#{floor} and room=#{room}")
    int updateRoomStatus(Map map);

    /**
     * @Description: 修改用户入住状态
     * @Author zys
     * @Date 2020/6/23 0023 13:26
     * @Param [userId]
     */
    @Update("update sys_user set checkInStatus=1 where workId=#{userid}")
    int changeUserCheckinStatus(Map map);
}
