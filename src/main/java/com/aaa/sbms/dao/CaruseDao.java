package com.aaa.sbms.dao;


import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface CaruseDao {
    /**
     * 查询所有111
     * @param map
     * @return
     */
    @Select("<script>select ca.*,u.username,c.carbyte,c.numplate,c.oilmass from sys_car_audit ca \n" +
            "left join sys_user u on ca.user_id=u.user_id left join sys_car c on c.carid=ca.carid\n" +
            "where ca.rState=1" +
            "<if test=\"username!=null and username!=''\"> and username like concat('%',#{username},'%')</if>" +
            "<if test=\"carbyte!=null and carbyte!=''\"> and carbyte like concat('%',#{carbyte},'%')</if></script>")
    List<Map> selectAllUse(Map map);

    /**
      * @Description: 维修
      * @Author zys
      * @Date 2020/7/1 0001 4:38
      * @Param [map]
     */
    @Select("<script>select ca.*,u.username,car.numplate,carbyte ,car.damagelevel \n" +
            "from sys_carmaintain ca left join sys_user u on ca.user_id=u.user_id \n" +
            "left join sys_car car on car.carid=ca.carid where ca.state=1\n" +
            "<if test=\"username!=null and username!=''\"> and username like concat('%',#{username},'%')</if>\n" +
            "<if test=\"carbyte!=null and carbyte!=''\"> and carbyte like concat('%',#{carbyte},'%')</if></script>")
    List<Map> listRepair(Map map);
}
