package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface HouseDao {

    /**
      * @Description:获取宿舍信息
      * @Author zys
      * @Date 2020/6/19 0019 12:35
      * @Param [map]
      * @return java.util.List<java.util.Map>
     */
    @Select("<script>select * from sys_houses" +
           "<if test=\"provinceValue!=null and provinceValue!=''\"> where address like concat('%',#{provinceValue},'%')</if>" +
           "<if test=\"cityValue!=null and cityValue!=''\"> and address like concat('%',#{cityValue},'%')</if>\n" +
           "<if test=\"areaValue!=null and areaValue!=''\"> and address like concat('%',#{areaValue},'%')</if>\n</script>")
    List<Map> listHouse(Map map);

    /**
      * @Description: 添加宿舍
      * @Author zys
      * @Date 2020/6/19 0019 16:40
      * @Param map
     */
    @Insert("insert into sys_houses values(null,#{ADDRESS1},#{type})")
    int addHouse(Map map);

    /**
      * @Description: 更新宿舍
      * @Author zys
      * @Date 2020/6/19 0019 20:36
      * @Param [map]
     */
    @Update("update sys_houses set address=#{ADDRESS1},type=#{type} where houseId=#{houseId}")
    int updateHouse(Map map);

    /**
      * @Description: 查询每间宿舍的楼层
      * @Author zys
      * @Date 2020/6/19 0019 21:45
      * @Param [houseId]
     */
    @Select("select distinct floor,houseId from sys_houses_items where houseId=#{houseId}")
    List<Map> listFloor(Integer houseId);

    /**
      * @Description: 获取房间号
      * @Author zys
      * @Date 2020/6/20 0020 10:53
      * @Param [floorId, houseId]
     */
    @Select("select room,status,bed from sys_houses_items where houseId=#{houseId} and floor =#{floorId}")
    List<Map> listRooms(Integer floorId,Integer houseId);

    /**
      * @Description: 添加楼层
      * @Author zys
      * @Date 2020/6/20 0020 15:21
      * @Param [map]
     */
    @Insert("insert into sys_houses_items values(null,#{houseId},#{floor},#{room},#{bed},#{status})")
    int addFloor(Map map);

    /**
      * @Description: 获取床位号
      * @Author zys
      * @Date 2020/6/21 0021 13:03
      * @Param [houseId, floorId, roomId]
     */
    @Select("select id, bedId,status from sys_bed where houseId=#{houseId}  and floorId=#{floorId} and roomId=#{roomId}")
    List<Map> listBeds(Integer houseId, Integer floorId, Integer roomId);

    /**
      * @Description: 判断当前添加的楼层房间是否存在
      * @Author zys
      * @Date 2020/6/21 0021 15:36
      * @Param [map]
     */
    @Select("select id from sys_houses_items where houseId=#{houseId} and floor=#{floor} and room=#{room}")
    int isListFloor(Map map);

    /**
      * @Description: 添加床位
      * @Author zys
      * @Date 2020/6/21 0021 14:20
      * @Param [map, bedId]
     */
    @Insert("insert into sys_bed values(null,#{houseId},#{floor},#{room},#{bedId},#{status})")
    int addBeds(Map map );

    /**
      * @Description: 删除房间床位
      * @Author zys
      * @Date 2020/6/21 0021 15:59
      * @Param [map]
     */
    @Delete("delete from sys_bed where houseId=#{houseId} and floorId=#{floor} and roomId=#{room}")
    int deleteBeds(Map map);

    /**
     * @Description: 根据houseId 查询楼层房间
     * @Author zys
     * @Date 2020/6/21 0021 16:16
     * @Param [houseId]
     */
    @Select("select id from sys_houses_items where houseId=#{houseId}")
    List<Map> listHouseItemsByHouseId(Integer houseId);


    /**
      * @Description: 根据宿舍ID删除宿舍
      * @Author zys
      * @Date 2020/6/21 0021 16:18
      * @Param [houseId]
     */
    @Delete("delete from sys_houses where houseId=#{houseId}")
    int  deleteHouseByHouseId(Integer houseId);

    /**
      * @Description: 修改床位状态
      * @Author zys
      * @Date 2020/6/21 0021 17:31
      * @Param [id]
     */
    @Update("update sys_bed set status=#{status} where id=#{id}")
    int changeStatus(Integer id,Integer status);

    /**
      * @Description: 获取房间主键
      * @Author zys
      * @Date 2020/6/23 0023 21:02
      * @Param [map]
     */
    @Select("select id from sys_houses_items  where houseId=#{houseId} and floor=#{floor} and room=#{room}")
    int getHIdByHFR(Map map);

    /**
      * @Description: 添加维修表信息
      * @Author zys
      * @Date 2020/6/23 0023 21:00
      * @Param [map]
     */
    @Insert("insert into sys_houserepaire values(null,#{remark},#{workId},now(),0,#{hId})")
    int addHouseRepair(Map map);

    /**
      * @Description: 添加维修
      * @Author zys
      * @Date 2020/6/23 0023 20:50
      * @Param [map]
     */
    @Update("update sys_houses_items set status=2 where houseId=#{houseId} and floor=#{floor} and room=#{room}")
    int addRepair(Map map);
}
