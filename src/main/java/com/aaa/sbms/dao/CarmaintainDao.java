package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface CarmaintainDao {
    /**
     * 查询所有
     * @param map
     * @return
     */
    @Select("<script>select  m.*,c.damagelevel,c.carbyte,c.numplate\n" +
            "from sys_carmaintain m left join sys_car c on c.carid=m.carid where m.state=0" +
            "<if test=\"carbyte!=null and carbyte!=''\"> and carbyte like concat('%',#{carbyte},'%')</if></script>")
   List<Map> selectAllList(Map map);

    /**
     * 删除
     * @param mainid
     * @return
     */
    @Delete("delete from sys_carmaintain where mainid = #{mainid}")
    int deleteMainId(Integer mainid);

    /**
     * 添加
     * @param map
     * @return
     */
    @Insert("insert into sys_carmaintain (expenditure,maintaintime,carid) values(#{expenditure},#{maintaintime},#{carid})")
    int addMain(Map map);

    /**
     * 通过id查询车辆id和名称
     * @return
     */
    @Select("select distinct c.carid,c.carbyte from sys_car c left join " +
            "sys_carmaintain a on a.carid=c.carid ")
    List<Map> getCar();

    /**
     * 更新
     * @param map
     * @return
     */
    @Update("update sys_carmaintain set expenditure=#{expenditure},maintaintime=#{maintaintime},carid=#{carid} where mainid=#{mainid}")
    int updateMain(Map map);

    /**
      * @Description: 车辆报修
      * @Author zys
      * @Date 2020/7/1 0001 2:19
      * @Param [map]
     */
    @Insert("insert into sys_carmaintain values(null,#{user_id},null,#{carid},null,0)")
    int addMaintain(Map map);

    /**
      * @Description: 修改维修状态
      * @Author zys
      * @Date 2020/7/1 0001 2:24
      * @Param [map]
     */
    @Update("update sys_car set carstate=3,damagelevel=#{damagelevel} where carid=#{carid}")
    int updateMaintainState(Map map);

    /**
      * @Description: 维修
      * @Author zys
      * @Date 2020/7/1 0001 2:57
      * @Param [map]
     */
    @Update("update sys_carmaintain set expenditure=#{expenditure},maintaintime=now(),state=1 where  mainid=#{mainid}")
    int toMaintain(Map map);


    /**
      * @Description: 修改维修后的车辆状态
      * @Author zys
      * @Date 2020/7/1 0001 3:07
      * @Param [map]
     */
    @Update("update sys_car set carstate=1 where carid=#{carid}")
    int updateState(Map map);
}
