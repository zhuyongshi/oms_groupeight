package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface ResultDao {

    @Select("<script>SELECT r.pId,r.sex,r.pName,r.r_result,r.cp_id,cp.cp_name FROM sys_recruit_record r left  join sys_courseplan cp on  r.cp_id=cp.cp_id where r.result = 0 "+
            "<if test=\"tName!=null and tName!=''\"> and r.pName like concat('%',#{tName},'%') or cp.cp_name like concat('%',#{tName},'%')</if>"+
            "</script>")
    List<Map> getAll(Map map);

    @Update("update sys_recruit_record  set cp_id = #{cp_id},r_result=#{r_result}  where pId= #{pId} ")
    int update(Map map);
}
