package com.aaa.sbms.dao;

import com.aaa.sbms.entity.Emp;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * fileName:UserDao
 * description:
 * author:zz
 * createTime:2019/11/26 16:37
 * version:1.0.0
 */
public interface UserDao {
    /**
      * 功能描述
      * @Author zys
      * @Description 获取员工信息
      * @Date 2020/6/11 0011 14:33
      * @Param []
      * @return java.util.List<java.util.Map>
     */
    @Select("<script>select u.user_id, u.username ,u.workId,u.leadId,u.groupId dId,d.dname,u.idCard,\n" +
            "s.username leader,u.isLock,u.lockDay,u.sex,u.tel,u.`status`\n" +
            "from sys_user u left join sys_user s on u.leadId=s.workId \n" +
            "left join dept d on d.deptId=u.groupId where u.status in (0,2)"+
            "<if test=\"userName!=null and userName!=''\"> and u.username like concat('%',#{userName},'%')</if>" +
            "<if test=\"sex!=null and sex!=''\"> and u.sex = #{sex}</if>\n" +
            "<if test=\"status!=null and status!=''\"> and u.status = #{status}</if>\n" +
            "<if test=\"dId!=null and dId!=''\"> and u.groupId = #{dId}</if>\n</script>")
    List<Map> getEmpList(Map map);

    /**
     * 根据用户名查询用户信息（登录使用）
     * @param tel
     * @return
     */
    @Select("select * from sys_user where status in (0,2) and tel=#{tel}")
    Map  getUserByUserName(String tel);

    /**
     * 根据条件查询
     * @param map
     * @return
     */
    @Select("<script>select user_id ,username from sys_user where status in(0,2)" +
            "<if test=\"userName!=null and userName!=''\"> and username like concat('%',#{userName},'%')</if> </script>")
    List<Map> list(Map map);

    /***
     * 添加
     * @param map
     * @return
     */
    @Insert("insert into sys_user (username,password,salt,status,workId,tel,\n" +
            "sex,isLock,lockDay,groupId,idCard,leadId) \n" +
            "values(#{username},#{password},#{salt},#{status},#{workId},#{tel},\n" +
            "#{sex},#{isLock},#{lockDay},#{dId},#{idCard},#{leadId})")
    int add(Map map);

    /**
     * 更新
     * @param map
     * @return
     */
    @Update("update sys_user set\n" +
            "username=#{username},status=#{status},leadId=#{leadId},tel=#{tel},sex=#{sex},\n" +
            "isLock=#{isLock},lockDay=#{lockDay},groupId=#{dId},idCard=#{idCard} \n" +
            "where user_id=#{user_id}")
    int update(Map map);

    /**
     * 删除
     * @param userId
     * @return
     */
    @Update("update sys_user set status=1 where user_id=#{userId}")
    int delete(int userId);

    /**
     * 查询用户id对应的角色id集合
     * @param userId
     * @return
     */
    @Select("select role_id from sys_user_role where user_id=#{userId}")
    List<Integer>  listRoleIdsByUserId(int userId);

    /**
     * 删除原有用户ID关联的所有角色
     * @param userId
     * @return
     */
    @Delete("delete from sys_user_role where user_id=#{userId}")
    int delteRoleIdsByUserId(int userId);

    /**
     * 添加用户角色关联
     * @param values
     * @return
     */
    @Insert("insert into sys_user_role values ${values}")
    int addUserAndRole(String values);

    /**
      * @Description: 获取直系领导
      * @Author zys
      * @Date 2020/6/12 0012 19:44
      * @Param [deptId]
      * @return java.util.List<java.util.Map>
     */
    @Select("select username,workId from sys_user where groupId=#{deptId} ")
    List<Map> getLear(int deptId);

    /**
      * @Description: 修改用户头像
      * @Author zys
      * @Date 2020/6/16 0016 21:26
      * @Param [imageUrl]
      * @return int
     */
    @Update("update sys_user set avatar=#{imageUrl} where user_id=#{user_id}")
    int updateAvatar(String imageUrl,Integer user_id);

    /**
      * @Description: 获取用户头像
      * @Author zys
      * @Date 2020/6/16 0016 22:58
      * @Param [user_id]
      * @return java.util.List<java.util.Map>
     */
    @Select("select username,tel,birthday,avatar from sys_user where user_id=#{user_id}")
    Map getPerson(Integer user_id);

    /**
      * @Description:更新个人信息
      * @Author zys
      * @Date 2020/6/17 0017 15:24
      * @Param [map]
      * @return int
     */
    @Update("update sys_user set username=#{username},tel=#{tel},birthday=#{birthday} where user_id=#{user_id}")
    int updatePerson(Map map);

    /**
      * @Description: 修改密码
      * @Author zys
      * @Date 2020/6/17 0017 16:09
      * @Param [map]
      * @return int
     */
    @Update("update sys_user set password=#{password},salt=#{salt} where user_id=#{user_id}")
    int updatePwd(Map map);


    /**
      * @Description: 获取用户角色
      * @Author zys
      * @Date 2020/6/17 0017 20:50
      * @Param []
      * @return java.util.List<java.util.Map>
     */
    @Select("select r.role_id,r.role_name roleName from sys_user_role ur \n" +
            "left join sys_role r on ur.role_id=r.role_id where user_id=#{user_id}")
    List<Map> getRoleByUserId(Integer user_id);

    /**
     * 下载查询
     * @return
     */
    @Select("select u.workId,u.username,u.status,u.tel,u.sex,d.dname,u.lockDay,u.idCard \n" +
            "from sys_user u left join dept  d on u.groupId=d.deptId")
    @Results({@Result(column = "dname",property = "groupId")})
    List<Emp> getAllEmp();
}
