package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface QuitDao {

    /**
      * @Description: 获取离职记录
      * @Author zys
      * @Date 2020/6/24 0024 15:21
      * @Param [map]
     */
    @Select("<script>select q.*,d.dname dName from sys_quit q left join dept d on q.dId=d.deptId \n" +
            "where q.checkState in(0,2)" +
            "<if test=\"userName!=null and userName!=''\"> and q.username like concat('%',#{userName},'%')</if>order by q.id</script>")
    List<Map> listQuit(Map map);

    /**
      * @Description: 获取待审核离职信息
      * @Author zys
      * @Date 2020/6/24 0024 17:02
      * @Param [map]
     */
    @Select("<script>select q.*,d.dname dName from sys_quit q left join dept d on q.dId=d.deptId \n" +
            "where q.checkState=1 " +
            "<if test=\"userName!=null and userName!=''\"> and q.username like concat('%',#{userName},'%')</if>order by q.id</script>")
    List<Map> listsRecord(Map map);

    /**
      * @Description: 通过审批
      * @Author zys
      * @Date 2020/6/24 0024 17:27
      * @Param [map]
     */
    @Update("update sys_quit set checkState=0,checkTime=now(),checkResult=#{checkResult} where id=#{id}")
    int updateCheckState(Map map);

    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/24 0024 17:33
      * @Param [map]
     */
    @Update("update sys_quit set checkState=2,checkTime=now(),checkResult=#{checkResult} where id=#{id}")
    int changeCheckState(Map map);

    /**
      * @Description: 提交离职申请
      * @Author zys
      * @Date 2020/6/29 0029 13:31
      * @Param [map]
     */
    @Insert("insert into sys_quit \n" +
            "values(null,#{workId},#{username},#{male},#{tel},#{groupId},#{idCard},#{lockDay},\n" +
            "now(),#{reason},1,null,null)")
    int toLeave(Map map);
}
