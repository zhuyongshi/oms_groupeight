package com.aaa.sbms.dao;

import com.aaa.sbms.entity.Teacher;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 培训讲师Dao
 * Fong
 */
public interface TeacherDao {




    /**
     * 查询所有
     * @return
     */
    @Select("<script> select t_id,t_name,t_sex,t_info,t_status,create_uid,date_format(create_time,'%Y-%m-%d %H:%i:%S') create_time,update_user,date_format(update_time,'%Y-%m-%d %H:%i:%S') update_time from sys_teacher"+
            "<if test=\"tName!=null and tName!=''\"> where t_name like concat('%',#{tName},'%')</if>" +
            " </script>")
    List<Map> selectAllTeacher(Map map);

    /**
     * 下载查询
     * @return
     */
    @Select("select t_id,t_name,t_sex,t_info,t_status from sys_teacher")
    @Results({
            @Result(column = "t_name",property = "name"),
            @Result(column = "t_sex",property = "sex"),
            @Result(column = "t_info",property = "info"),
            @Result(column = "t_status",property = "status")
    })
    List<Teacher> getAllTeacher();


    /**
     * 根据教师ID删除数据
     * @param teacherId
     * @return
     */
    @Delete("delete from sys_teacher where t_id=#{teacherId}")
    int deleteTeacherById(int teacherId);

    /**
     * 添加教师信息
     * @param map
     * @return
     */
    @Insert("INSERT INTO sys_teacher ( t_name, t_sex, t_info, t_status, create_uid, create_time,update_user,update_time )" +
            "VALUES" +
            "(#{t_name},#{t_sex},#{t_info},1,#{userName},now(),#{userName},now());")
    int addTeacher(Map map);

    /**
     * 编辑教师信息
     * @param map
     * @return
     */
    @Update("update sys_teacher set t_name=#{t_name}, t_sex=#{t_sex}, t_info=#{t_info}, t_status=#{t_status},update_user=#{userName}, update_time=now() where t_id=#{t_id} ")
    int updateTeacher(Map map);

    @Select("select t_id, t_name from sys_teacher where t_status=1")
    List<Map> getTeacherName();

    @Update("update sys_teacher set t_status=#{status} where t_name=#{tName}")
    int changeTeacherStatus(String tName,Integer status);
}
