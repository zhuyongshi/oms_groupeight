package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface AutidDao {
    /**
     * 查询所有
     * @param map
     * @return
     */
    @Select("<script>select ac.auditid,ac.starttime,ac.starttime,ac.userason,u.username,c.* \n" +
            "from sys_car_audit ac left join sys_car c on c.carid= ac.carid \n" +
            "left join sys_user u on ac.user_id=u.user_id where auditstate=0" +
            "<if test=\"username!=null and username!=''\"> and username like concat('%',#{username},'%')</if>"+
            "<if test=\"carbyte!=null and carbyte!=''\"> and carbyte like concat('%',#{carbyte},'%')</if></script>")
    List<Map> selectAllAutid(Map map);


    /**
     * 更新
     * @param map
     * @return
     */
    @Update("update sys_car_audit set result=#{result},roleid=#{user_id},auditstate=1 where auditid=#{auditid}")
    int updateAutidId(Map map);
    /**
     * 通过id查询用户id和名称
     * @return
     */
    @Select("select distinct u.user_id,u.username from sys_user u " +
            "left join sys_car_audit a on a.user_id = u.user_id ")
    List<Map> getUser();
    /**
     * 通过id查询车辆id和名称
     * @return
     */
    @Select("select c.carid,c.carbyte from sys_car c left join " +
            "sys_car_audit a on a.carid=c.carid where c.damagelevel=0")
    List<Map> getCar();

    /**
      * @Description: 驳回
      * @Author zys
      * @Date 2020/6/30 0030 23:08
      * @Param [map]
     */
    @Update("update sys_car_audit set result=#{result},roleid=#{user_id},auditstate=2 where auditid=#{auditid}")
    int unPass(Map map);

    /**
      * @Description:
      * @Author zys
      * @Date 2020/6/30 0030 23:27
      * @Param [map]
     */
    @Update("update sys_car set carstate=2 where carid=#{carid}")
    int updateUseStatus(Map map);

    /**
      * @Description: 获取当前用户借用的车辆
      * @Author zys
      * @Date 2020/6/30 0030 23:37
      * @Param []
     */
    @Select("select c.*,ca.starttime,ca.auditid from sys_car_audit ca \n" +
            "left join sys_car c on ca.carid=c.carid\n" +
            "where ca.rState=0 and ca.user_id=#{user_id} and c.carstate=2")
    Map getUserCarByUserId(Integer user_id);

    /**
      * @Description: 归还车辆
      * @Author zys
      * @Date 2020/7/1 0001 0:27
      * @Param [map]
     */
    @Update("update sys_car_audit set rState=1,endtime=now() where auditid=#{auditid}")
    int updaterState(Map map);

    /**
      * @Description: 更新车辆使用状态
      * @Author zys
      * @Date 2020/7/1 0001 1:45
      * @Param [map]
     */
    @Update("update sys_car set carstate=1 where carid=#{carid}")
    int updateCarStatus(Map map);
}
