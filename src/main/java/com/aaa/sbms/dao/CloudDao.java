package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface CloudDao {

    /**
     * 上传文件
     * @param oldFileName
     * @param userId
     * @param newFilePath
     * @return
     */
    @Insert("insert into sys_file (f_name,user_id,path,upload_time)"+
            "values"+
            "(#{oldFileName},#{userId},#{newFilePath},now())")
    int upload(String oldFileName,int userId,String newFilePath);

    /**
     * 查询文件
     * @param userId
     * @return
     */
    @Select("select f_id, f_name,date_format(upload_time,'%Y-%m-%d')upload_time from sys_file where user_id=#{userId} and f_status = 1")
    List<Map> getFileList(int userId);

    /**
     * 文件名查询
     * @param fileId
     * @return
     */
    @Select("select f_id, f_name, path from sys_file where f_id=#{fileId} and f_status = 1")
    Map getFileByid(int fileId);

    /**
     * 重命名
     * @param map
     * @return
     */
    @Update("update sys_file set f_name=#{f_name} where f_id=#{f_id} and f_status = 1")
    int updateFileName(Map map);

    /**
     * 把文件移到回收站  1 正常文件  2 回收站文件
     * @param fileId
     * @return
     */
    @Update("update sys_file set f_status=2,delete_time=now() where f_id=#{fileId} and f_status = 1")
    int deleteFile(Integer fileId);
}
