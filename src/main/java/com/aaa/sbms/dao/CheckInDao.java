package com.aaa.sbms.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface CheckInDao {

    /**
      * @Description: 查询入住员工信息
      * @Author zys
      * @Date 2020/6/22 0022 15:24
      * @Param [map]
     */
    @Select("<script>select user_id,username,workId,isLock,sex from sys_user where status in (0,2) and checkInStatus=1 \n" +
            "<if test=\"userName!=null and userName!=''\"> and username like concat('%',#{userName},'%')</if></script>")
    List<Map> listCheckIn(Map map);

    /**
      * @Description: 修改入住意向
      * @Author zys
      * @Date 2020/6/22 0022 16:04
      * @Param [userId]
     */
    @Update("update sys_user set isLock=#{isLock} where user_id=#{userId}")
    int changeIsLock(Integer userId,Integer isLock);

    /**
      * @Description: 根据性别获取宿舍地址
      * @Author zys
      * @Date 2020/6/22 0022 16:53
      * @Param [sex]
     */
    @Select("select * from sys_houses where type=#{sex}")
    List<Map> listHouses(Integer sex);

    /**
      * @Description: 根据宿舍获取楼层
      * @Author zys
      * @Date 2020/6/22 0022 17:23
      * @Param [houseId]
     */
    @Select("select  houseId,floor from sys_houses_items where houseId=#{houseId} GROUP BY floor")
    List<Map> getFloors(Integer houseId);

    /**
      * @Description: 根据楼层获取房间
      * @Author zys
      * @Date 2020/6/22 0022 17:49
      * @Param [houseId, floor]
     */
    @Select("select room from sys_houses_items where houseId=#{houseId} and floor=#{floor} and status=0")
    List<Map> getRooms(Map map);

    /**
      * @Description: 根据房间号获取床位号
      * @Author zys
      * @Date 2020/6/22 0022 22:10
      * @Param [map]
     */
    @Select("select id bId,bedId from sys_bed where houseId=#{houseId} and floorId=#{floor} and roomId=#{room} and status=0")
    List<Map> getBeds(Map map);

    /**
      * @Description: 添加入住信息
      * @Author zys
      * @Date 2020/6/22 0022 22:22
      * @Param [map]
     */
    @Insert("insert into sys_checkin values(null,#{workId},#{username},#{houseId},#{floor},#{room},#{bedId},now(),null,#{InsertWorkId},#{sex},0)")
    int checkInInfo(Map map);

    /**
      * @Description: 修改床位状态
      * @Author zys
      * @Date 2020/6/22 0022 22:50
      * @Param [map]
     */
    @Update("update sys_bed set status=1 where id=#{bId}")
    int changeBedStatus(Map map);

    /**
      * @Description: 修改房间状态
      * @Author zys
      * @Date 2020/6/22 0022 22:55
      * @Param [map]
     */
    @Update("update sys_houses_items set status=1 where houseId=#{houseId} and floor=#{floor} and room=#{room}")
    int changeRoomsStatus(Map map);

    /**
      * @Description: 修改用户入住状态
      * @Author zys
      * @Date 2020/6/23 0023 13:26
      * @Param [userId]
     */
    @Update("update sys_user set checkInStatus=0 where user_id=#{userId}")
    int changeUserCheckinStatus(Integer userId);
}
