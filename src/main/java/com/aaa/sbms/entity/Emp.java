package com.aaa.sbms.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;

/**
 * fileName:Emp
 * description:
 * author:zz
 * createTime:2019/11/26 15:40
 * version:1.0.0
 */
public class Emp {

    @ColumnWidth(20)
    @ExcelProperty("工号")
    private String workId;
    @ColumnWidth(20)
    @ExcelProperty("姓名")
    private String username;

    @ColumnWidth(20)
    @ExcelProperty("状态")
    private String status;
    @ColumnWidth(20)
    @ExcelProperty("电话")
    private String tel;
    @ColumnWidth(20)
    @ExcelProperty("性别")
    private String sex;
    @ColumnWidth(20)
    @ExcelProperty("身份证号")
    private String idCard;
    @ColumnWidth(20)
    @ExcelProperty("部门")
    private String groupId;
    @ColumnWidth(20)
    @ExcelProperty("入职日期")
    private String lockDay;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null? null: status.trim();
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getLockDay() {
        return lockDay;
    }

    public void setLockDay(String lockDay) {
        this.lockDay = lockDay;
    }

}
