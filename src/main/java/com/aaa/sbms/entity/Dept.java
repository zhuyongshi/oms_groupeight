package com.aaa.sbms.entity;

/**
 * fileName:Dept
 * description:
 * author:zz
 * createTime:2019/11/26 15:40
 * version:1.0.0
 */
public class Dept {

    private String dname;
    private String loc;

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }
}
