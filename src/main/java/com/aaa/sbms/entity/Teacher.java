package com.aaa.sbms.entity;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 培训讲师实体类
 * Fong
 */
public class Teacher {
    //培训讲师ID
    @ExcelIgnore
    @ExcelProperty("ID")
    private Long id;
    //培训讲师名字
    @ColumnWidth(20)
    @ExcelProperty("姓名")
    private String name;
    //性别
    @ColumnWidth(20)
    @ExcelProperty("性别")
    private String sex;
    //简介
    @ColumnWidth(40)
    @ExcelProperty("简介")
    private String info;
    //教师状态 1.空闲 2.在教 3.出差 4.辞职 5.假期
    @ColumnWidth(20)
    @ExcelProperty("状态")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null? null: status.trim();
    }

    //创建者ID
    @ExcelIgnore
    private Integer createId;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ExcelIgnore
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    public Integer getCreateId() {
        return createId;
    }

    public void setCreateId(Integer createId) {
        this.createId = createId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


}
