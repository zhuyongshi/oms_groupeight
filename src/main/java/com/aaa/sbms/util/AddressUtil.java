package com.aaa.sbms.util;

import java.util.Map;

/**
 * className:AddressUtil
 * description: 省市区三级联动地址拼接
 * author:zys
 * createTime:2020/6/19 0019 20:38
 */
public class AddressUtil {
    public  static  Map Addr(Map map){
        //获取省值
        String provinceValue = (String) map.get("provinceValue");
        //获取市值
        String cityValue = (String) map.get("cityValue");
        //获取区，县
        String areaValue =(String) map.get("areaValue");
        //获取详细地址
        String address =(String) map.get("ADDRESS");
        //拼接完整的地址
        String ADDRESS=provinceValue+","+cityValue+","+areaValue+","+address;
        map.put("ADDRESS1",ADDRESS);
        return map;
    }
}
