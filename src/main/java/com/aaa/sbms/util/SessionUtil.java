package com.aaa.sbms.util;

import org.apache.shiro.session.Session;

import java.util.Map;

/**
 * fileName:SessionUtil
 * description:
 * author:zz
 * createTime:2020/3/18 17:07
 * version:1.0.0
 */
public class SessionUtil {

    private  static Session sessionInstance;

    /**
     * 设置session
     * @param session
     */
    public static void setSession(Session session){
        sessionInstance=session;
    }


    /**
     * 获取用户信息
     * @return
     */
    public static Map getUserInfo(){
        if(sessionInstance!=null){
            return  (Map)sessionInstance.getAttribute("userInfo");
        }
        return null;
    }

    /**
     * 获取用户ID
     * @return
     */
    public static int getUserId(){
        if(sessionInstance!=null){
            return  Integer.valueOf(((Map)sessionInstance.getAttribute("userInfo")).get("user_id")+"");
        }
        return 0;
    }

    /**
     *获取用户名称
     * @return
     */
    public static String getUserName(){
        if(sessionInstance!=null){
            return  ((Map)sessionInstance.getAttribute("userInfo")).get("username")+"";
        }
        return "";
    }
    /**
      * @Description: 获取登录账号
      * @Author zys
      * @Date 2020/6/13 0013 11:38
      * @Param []
      * @return java.lang.String
     */
    public static String getTel(){
        if (sessionInstance!=null){
            return((Map)sessionInstance.getAttribute("userInfo")).get("tel")+"";
        }
        return "";
    }
}

