package com.aaa.sbms.util;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * fileName:FtpUtil
 * description:
 * author:zz
 * createTime:2020/4/20 11:35
 * version:1.0.0
 */
public class FtpUtil {

    /**
     * 通用上传方法
     *
     * @param ftp
     * @param file
     * @param savePath
     * @return
     */
    public static Map localUpload(FtpProperties ftp, MultipartFile file, String savePath) {
        //实例化map 用来做返回值，返回值中返回上传文件路径和名称
        Map map = new HashMap();
        try {
            //获取原上传文件名称   "redis主从复制原理.png"
            String originalFilename = file.getOriginalFilename();
            //获取原上传文件后缀  substring截取字符串(开始位置)    lastIndexOf（"."）从字符串后面查找第一个出现.的位置
            // .png
            String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
            //组装新文件名称   UUID.randomUUID() =xxx    xxx.png
            String fileName = UUID.randomUUID() + suffix;
            //调用Ftp上传
            uploadFile(ftp.getHost(), Integer.valueOf(ftp.getPort()), ftp.getUsername(), ftp.getPassword(),
                    ftp.getBasePath(), savePath, fileName, file.getInputStream());
            map.put("oldFileName", originalFilename);
            map.put("newFilePath", savePath + fileName);
            return map;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    /**
     * 真正的ftp上传文件方法（网上可以自己找帖子）
     * @param host
     * @param port
     * @param userName
     * @param password
     * @param basePath
     * @param filePath
     * @param fileName
     * @param input
     * @return
     * @throws IOException
     */
    private static Boolean uploadFile(String host, int port, String userName,
                                      String password, String basePath, String filePath, String fileName,
                                      InputStream input) throws IOException {

        FTPClient ftpClient = null;
        try {
            ftpClient = new FTPClient();
            //1,连接ftp服务
            ftpClient.connect(host, port);
            //2, 登录ftp
            ftpClient.login(userName, password);
            //230表示登录成功，
            int replyCode = ftpClient.getReplyCode();
            //3, 此方法200到300之间都返回true,表示连接和登录成功，返回false表示登录失败
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                ftpClient.disconnect();
                return false;
            }

            String tmpPath = "";
            //4, 检测目标目录是否存在，如果返回true不需要创建目录，否正需要创建目录
            //filePath images  basePath  /home/ftp/   filePath 自定义路径  a/b/c
            // storePath=/home/ftp/a/b/c
            String storePath = basePath + "/" + filePath;
            //判断目录是否存在，如果存在，切换到该目录，如果不存在创建目录
            if (!ftpClient.changeWorkingDirectory(storePath)) {
                // filePaht=a/b//c
                String[] split = filePath.split("/");
                tmpPath = basePath;
                for (String dir : split) {
                    if (null == dir || "".equals(dir)) {
                        continue;
                    }
                    tmpPath += "/" + dir;
                    //检测目录是否存在
                    if (!ftpClient.changeWorkingDirectory(tmpPath)) {
                        //makeDirectory是创建目录
                        if (!ftpClient.makeDirectory(tmpPath)) {
                            return false;
                        }
                    }

                }
            }
            //5, 把文件以字符流的形式上传
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            //6,真正的上传文件，true表示上传成功，false表示上传失败
            if (!ftpClient.storeFile(fileName, input)) {
                return false;
            }
            return  true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //7,关闭输入流
            input.close();
            //退出ftp登录
            ftpClient.logout();

            ftpClient.disconnect();
        }

        return false;
    }


    /**
     * 下载方法
     * @param ftp
     * @param fileName
     * @return
     */
    public static Boolean downloadFile(FtpProperties ftp,String fileName,OutputStream outputStream) {
        try {
            download(ftp.getHost(), Integer.valueOf(ftp.getPort()), ftp.getUsername(), ftp.getPassword(),
                    ftp.getBasePath(),fileName,outputStream);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 下载方法
     * @param host
     * @param port
     * @param userName
     * @param password
     * @param fileName
     * @param outputStream
     * @return
     * @throws IOException
     */
    private static Boolean download(String host, int port, String userName,
                                        String password, String basePath, String fileName,
                                        OutputStream outputStream)throws IOException{
        FTPClient ftpClient=null;
        try {
            //FTP连接部分
            ftpClient=new FTPClient();
            ftpClient.connect(host,port);
            ftpClient.login(userName,password);
            int replyCode=ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)){
                ftpClient.disconnect();
                return false;
            }

            //连接登录成功后下载方法
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.changeWorkingDirectory(basePath);
            if (!ftpClient.retrieveFile(fileName,outputStream)){
                return false;
            }
            return true;
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            outputStream.close();
            ftpClient.logout();
            ftpClient.disconnect();
        }
        return false;
    }

}
