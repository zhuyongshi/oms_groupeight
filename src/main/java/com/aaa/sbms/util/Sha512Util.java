package com.aaa.sbms.util;

import org.apache.shiro.crypto.hash.Sha512Hash;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * className:Sha512Util
 * description:
 * author:zys
 * createTime:2020/6/17 0017 16:41
 */
public class Sha512Util {
    public static Map pwd(String password){
        //盐值
        String salt = UUID.randomUUID().toString().substring(0,15);
        //随机盐值，放入数据库

        //获取用户输入的原始密码

        //使用SHA512加密算法，把原始密码和随机到的盐值一起进行运算，得到密码，存入数据
        //Sha512Hash(Object source, Object salt, int hashIterations)
        //          第1个参数，原始密码，第2参数，随机盐值  第3参数 hash次数
        //           和配置类中配置的加密算法名称和hash次数一致 com.aaa.sbms.config.ShiroConfig
        Sha512Hash sha512Hash = new Sha512Hash(password,salt,1024);
        Map map=new HashMap();
        map.put("salt",salt);
        map.put("password",sha512Hash.toString());
        return map;
    }
}
