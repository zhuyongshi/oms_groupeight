package com.aaa.sbms.util;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * className:PageUtil
 * description:
 * author:zys
 * createTime:2020/6/11 0011 16:25
 */
public class PageUtil {
    /**
      * 功能描述
      * @Author zys
      * @Description 分页方法
      * @Date 2020/6/11 0011 17:22
      * @Param [map]
      * @return java.util.Map
     */
    public static Map list(Map map) {
        Integer pageNumber = Integer.valueOf(map.get("pageNumber")+"");
        Integer pageSize = Integer.valueOf(map.get("pageSize")+"");
        System.out.println(pageNumber+"++"+pageSize);
        Page<Object> startPage = PageHelper.startPage(pageNumber, pageSize);
        Map map1=new HashMap();
        map1.put("offsetPage",startPage);
        return map1;
    }
}
