package com.aaa.sbms.util;


import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * className:FileUtil
 * description: 下载文件
 * author:zys
 * createTime:2020/6/26 0026 12:16
 */
public class FileUtil {

    /**
      * @Description: 文件下载
      * @Author zys
      * @Date 2020/6/26 0026 12:57
      * @Param [response, fileName]
     */

    public static ServletOutputStream   Download(HttpServletResponse response,String fileName) throws IOException {
        //编码
        response.setCharacterEncoding("utf-8");
        //文件名
        response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + ".xlsx\"");
        //把东西按流形式进行保存方便等下进行文件下载功能
        ServletOutputStream outputStream = response.getOutputStream();
        return outputStream;
    }

}
